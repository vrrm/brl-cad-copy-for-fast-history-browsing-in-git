<!-- Converted by db4-upgrade version 1.0 -->

<refentry xmlns="http://docbook.org/ns/docbook" version="5.0" xml:id="attr">
<refmeta>
  <refentrytitle>ATTR</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class="source">BRL-CAD</refmiscinfo>
  <refmiscinfo class="manual">BRL-CAD MGED Commands</refmiscinfo>
</refmeta>

<refnamediv xml:id="name">
  <refname>attr</refname>
  <refpurpose> Used to create, change, retrieve, or view attributes of database
objects.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv xml:id="synopsis">
  <cmdsynopsis sepchar=" ">
    <command>attr</command>    
    <arg choice="opt" rep="norepeat">get</arg>
    <arg choice="opt" rep="norepeat">set</arg>
    <arg choice="opt" rep="norepeat">rm</arg>
    <arg choice="opt" rep="norepeat">append</arg>
    <arg choice="opt" rep="norepeat">show</arg> 
    <arg choice="req" rep="norepeat"><replaceable>object_name</replaceable></arg>
    <arg choice="opt" rep="norepeat"><replaceable>arguments</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsection xml:id="description"><info><title>DESCRIPTION</title></info>
  
  <para>
    Used to create, change, retrieve, or view attributes of database
    objects. The arguments for "set" and "append" subcommands are attribute
    name/value pairs. The arguments for "get," "rm," and "show" subcommands are
    attribute names. The "set" subcommand sets the specified attributes for the object.
    The "append" subcommand appends the provided value to an existing attribute, or
    creates a new attribute if it does not already exist. The "get" subcommand retrieves
    and displays the specified attributes. The "rm" subcommand deletes the specified
    attributes. The "show" subcommand does a "get" and displays the results in a user
    readable format. Note that the attribute names may not contain embedded white
    space, and if attribute values contain embedded white space, they must be surrounded
    by {} or double quotes.
  </para>
</refsection>

<refsection xml:id="examples"><info><title>EXAMPLES</title></info>
  
  <para>
    The examples demonstrate the use of the <command>attr</command> command and subcommands to 
    assign and list attributes of database objects.
  </para>
  <example><info><title>Assigns an attribute to an object.</title></info>
    
    <variablelist>
      <varlistentry>
	<term><prompt>mged</prompt><userinput>attr set region_1 comment {This is a comment for region_1}</userinput></term>
	<listitem>
	  <para>
	    Assigns an attribute named "comment" to <emphasis>region_1</emphasis>. Its 
	    value is "This is a comment for region_1."
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
  </example>
  
  <example><info><title>List all the attributes for an object</title></info>
    
    <variablelist>
      <varlistentry>
	<term><prompt>mged</prompt><userinput>attr show region_1 comment</userinput></term>
	<para>Lists all the attributes for <emphasis>region_1</emphasis>.
	</para>
      </varlistentry>
    </variablelist>
  </example>
  
</refsection>

<info><corpauthor>BRL-CAD Team</corpauthor></info>

<refsection xml:id="bug_reports"><info><title>BUG REPORTS</title></info>
  
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsection>
</refentry>
