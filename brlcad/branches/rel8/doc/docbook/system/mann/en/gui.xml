<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='gui'>

<refmeta>
  <refentrytitle>GUI</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>gui</refname>
  <refpurpose>Creates an instance of MGED�s default Tcl/Tk graphical user
interface (GUI).
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>gui</command>    
    <group>
      <arg choice='req'>-config</arg>
      <arg>b</arg>
      <arg>c</arg>
      <arg>g</arg>
    </group>
    <group>
      <arg choice='req'>-d</arg> 
      <arg choice='req'><replaceable>display_string</replaceable></arg>
    </group>
    <group>
	<arg choice='req'>-gd</arg>
	<arg choice='req'><replaceable>graphics_display_string</replaceable></arg>
    </group>
    <group>
	<arg choice='req'>-dt</arg>
      <arg choice='req'><replaceable>graphics_type</replaceable></arg>
    </group>
    <group>
      <arg choice='req'>id</arg>
	<arg choice='req'><replaceable>name</replaceable></arg>
    </group>
    <group>
	<arg choice='req'>-c</arg>
	<arg choice='req'>-h</arg>
	<arg choice='req'>-j</arg>
	<arg choice='req'>-s</arg>
    </group>

  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Creates an instance of MGED�s default Tcl/Tk graphical user
	interface (GUI). The following options are allowed:
  </para>
  <variablelist>
          <varlistentry><term><emphasis>-config b c g</emphasis></term>
            <listitem>
              <para>Configures the GUI to display the command window, the graphics
			window, or both. This option is useful only when the GUI is
			combining the text and graphics windows. See the <emphasis>-c</emphasis> option.                    	        </para>
            </listitem>
          </varlistentry>
	    <varlistentry><term><emphasis>-d display_string</emphasis></term>
            <listitem>
              <para>Displays/draws the GUI on the screen indicated by the display_string.
			Note that this string format is the same as the X DISPLAY
			environment variable.	        
		  </para>
            </listitem>
          </varlistentry>
          <varlistentry><term><emphasis>-gd display_string</emphasis></term>
            <listitem>
              <para>Displays/draws the graphics window on the screen indicated by the
			display_string. Note that this string format is the same as the X
			DISPLAY environment variable.
		  </para>
            </listitem>
          </varlistentry>
	    <varlistentry><term><emphasis>-dt graphics_type</emphasis></term>
            <listitem>
              <para>Indicates the type of graphics windows to use. The possible
			choices are X and ogl (for machines that support OpenGL).
			Defaults to ogl, if supported; otherwise X.
		  </para>
            </listitem>
          </varlistentry>
	    <varlistentry><term><emphasis>-id name</emphasis></term>
            <listitem>
              <para>Specifies the id to use when referring to this instance of the GUI.
		  </para>
            </listitem>
          </varlistentry>
	    <varlistentry><term><emphasis>-c</emphasis></term>
            <listitem>
              <para>Combines text window and display manager windows.
		  </para>
            </listitem>
          </varlistentry>
		<varlistentry><term><emphasis>-s</emphasis></term>
            <listitem>
              <para>Uses separate text window and display manager windows. This is
			the default behavior.
		  </para>
            </listitem>
          </varlistentry>
	    <varlistentry><term><emphasis>-j</emphasis></term>
            <listitem>
              <para>Join the collaborative session.
		  </para>
            </listitem>
          </varlistentry>
	    <varlistentry><term><emphasis>-h</emphasis></term>
            <listitem>
              <para>Prints the help message.
		  </para>
            </listitem>
          </varlistentry>
	</variablelist>

</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>There were no examples given for this command.
  </para>
  
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

