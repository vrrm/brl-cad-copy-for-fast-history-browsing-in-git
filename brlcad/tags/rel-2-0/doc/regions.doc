.\" $Header$
;Date:     Fri, 24 Apr 87 9:58:56 EDT
;From: Keith Applin (VLD/VMB) <keith@BRL.ARPA>

About MGED objects.  The generic name given to any member of an MGED file
is object.  There are 2 divisions at the next level: primitives(solids) and
combinations.  The primitives are just as you might expect, the defining
parameters.  The combinations consist of members with a boolean operation.
The combinations are further divided into regions and groups.  A group
is a combination consisting of any objects along with only the union
operation.  A region is a combination of any objects (but generally only
primitives and other regions) along with any of the operations.  These
regions are special in that they are ALL that the ray-tracing evaluates
or sees.  Hence every path must contain a region for ray-tracing purposes.
If a path does not have a region in it, then when ray-traced, a region will
be created consisting of only the solid at the bottom of the path.
The regions also have a material code number attached, to be used by some
of the ray-tracing applications.

The procedure generally followed in building a model is:

	1.  Build the primitives at desired locations, etc
	2.  Make the regions of these solids to produce the
	    desired shape.
	3.  Make higher level groups of these regions,
	    generally according to function, etc
            NOTE: using this procedure, the regions
            cannot occupy the same volume in space(overlap)

I got your "castle" today and took a quick look at castle.g.  The use of
all regions (using the union operation) allows just about everything to
overlap (interfere).  The "E" command in MGED does not know how to
evaluate regions within regions, hence you must use the "e" command to
display such animals.

I didn't get a chance to look at castle-gr.g, but I assume you used groups
(non-region combinations) to define the higher levels instead of regions.
Of course, once you do this, any overlaps between the lower level regions
will show up during ray-tracing.

Both ways of doing business are okay, except you must be careful about
overlapping regions when higher levels are made using groups.  I guess
it is a case of personal preference.

;Date: Sun, 26 Apr 87 18:51:29 edt
;From: Mike McDonnell <mike>

Thanks for your clear explanation of the differences between
regions and groups.  I now understand some of the differences
between the two types of combination objects.  I also still don't
*like* those differences, at least in some respects.  For example,
if the raytracer only sees regions then why can't MGED evaluate
[E] hierarchies of regions?  This is a nice way to preview a
model in detail before you raytrace it.  Much cheaper than
raytracing, and much better than 'e' evaluation for complex
objects.  Is there a good reason for this behavior?  As a
newcomer to the CAD package I can maybe see the ugly aspects of
it better than some of you "old hands" who have come to accept
its peculiarities.  In fact I still see no good reason to have
such an entity as a group at all.  Why not make a group a special
case of a region and make regions equally acceptable to all the
various programs in the CAD package?  If the purpose of groups is
to have a way to check that nothing overlaps, then it seems to me
that this can be checked by special commands operating on
regions.  If the "purpose" of groups is that they are historical
baggage, then they should be excised as a warty complication of
what could be an elegant system in which there are only solids
and regions with solids as leaf nodes and regions for all
interior nodes.

;Date:     Mon, 27 Apr 87 13:39:32 EDT
;From: "Gary S. Moss (SLCBR-VLD-V)" <moss@BRL.ARPA>

 For our mission, we require there be groups as such.  Regions are used
to describe low-level functional units of homogeneous material composition.
Groups are used to combine regions in order to assemble higher level functional
units of heterogeneous material composition or to describe application-specific
lists of objects.  As far as the software is concerned, groups are not
boolean combinations, but simple lists of objects.  That is why these pseudo
unions will result in overlaps if the 2 members of a group occupy the same
space.  If groups WERE evaluated as unions, it would incur considerable CPU
overhead, and would encourage mis-use of this construct.  That is, someone
could model two road wheels that happened to overlap (due to modeling
error) and then group them together to form a "road_wheels" group, which
would take care of the overlap by unioning them together.  Typically, we
group most of the description into an application-specific entity like
"exterior" for doing a certain type of analysis.  Now, if groups were
really unions of objects, nothing could possible overlap.  For our mission,
we MUST have accurate geometry, and this would allow errors to go unnoticed.

The region evaluator in MGED is deficient in that it does not handle
regions within regions.  We have always known this, the code that does
the region evaluation was taken from the CIDD code which does not
support regions within regions.  It has been improved somewhat since it
was grafted onto MGED, but we intend to rewrite it eventually.  This
project is not high on our priority list.

The RT library handles regions within regions fine, but since they are
supposed to be of homogeneous material composition, the upper-most
region material ID takes precedence.  Perhaps the regions could be
modified to include the group as a special case, but this does not sound
"cleaner" to me given the functional distinctions that I have
mentioned.  I think "historical baggage" is not an accurate description
of groups, because they are much cheaper then regions and have a
distinct purpose.

~moss/...

;Date:     Wed, 29 Apr 87 2:01:37 EDT
;From: Mike Muuss <mike@BRL.ARPA>

The "problem" with regions is more of a definitional one.  Internal to
the software, there are only two types of nodes, non-terminal ("combinations")
and terminal ("solids").  It is possible to set a flag on a non-terminal
node marking it as a region.

The problem comes from the fact that everything above the region flagged node
(towards the root) is used for grouping, where as everything below the
region flagged node is actually defining a single, solid, self-consistent
chunk of 3-d space.  Therefore, regions are chunks of 3-space, and
regions are grouped together to form the model.  It is permitted in RT,
but not in some earlier software, to perform non-union operations on
regions (ie, subtract one region from another).

The "big-E" command in MGED is slated for overhaul.  The task of
"facetizing" a CSG model is a difficult one;  the current code in MGED,
while very useful, is quite inadequate.  I had hoped to have created
a general capability for facetizing MGED databases last year, but lots
of things (most importantly the Cray, and the CAD Distribution itself)
kept getting prioritized ahead of that project, and it still languishes.

	Best,
	 -Mike
  
