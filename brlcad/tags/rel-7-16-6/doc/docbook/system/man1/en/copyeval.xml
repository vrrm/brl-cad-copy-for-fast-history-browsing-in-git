<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='copyeval'>
  
  <refmeta>
    <refentrytitle>COPYEVAL</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>copyeval</refname>
    <refpurpose>Creates a new primitive object called <emphasis>new_ primitive</emphasis>by 
    applying the transformation matrices accumulated along the 
    <emphasis>path_to_old_primitive</emphasis> to the leaf primitive shape object at the end 
    of the path and saving the result under the name <emphasis>new_ primitive</emphasis>.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>copyeval</command>    
      <arg choice='req'> <replaceable>new_primitive path_to_old_primitive</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>Objects in a <emphasis>BRL-CAD</emphasis> model are stored as Boolean trees 
    (combinations), with the members being primitive shapes or other Boolean trees. Each 
    member has a transformation matrix associated with it. This arrangement allows a 
    primitive to be a member of a combination, and that combination may be a member of another
    combination, and so on. When a combination is displayed, the transformation
    matrices are applied to its members and passed down through the combinations to the
    leaf (primitive shape) level. The accumulated transformation matrix is then applied
    to the primitive before it is drawn on the screen. The <command>copyeval</command> command 
    creates a new primitive object called <emphasis>new_ primitive</emphasis> by applying the 
    transformation matrices accumulated along the <emphasis> path_to_old_primitive</emphasis> to 
    the leaf primitive shape object at the end of the path and saving the result under the name 
    <emphasis>new_ primitive.</emphasis> The <emphasis> path_to_old_ primitive </emphasis> must 
    be a legitimate path ending with a primitive shape.
    </para>
  </refsect1>

  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      The example shows the use of the <command>copyeval</command> command to create 
      a new primitive by applying the transformation matrices along the path to the old 
      primitive. 
    </para>
    <example>
      <title>Create a new primitive object by applying the transformtion matrices along the path
      to the old primitive.</title>
      <para><prompt>mged></prompt> <userinput>copyeval shapeb comb1/comb2/comb3/shapea</userinput></para>
      <para>Creates <emphasis>shapeb</emphasis> from <emphasis>shapea</emphasis> by applying the accumulated transformation matrices from the path comb1/comb2/comb3.</para>
    </example>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>
