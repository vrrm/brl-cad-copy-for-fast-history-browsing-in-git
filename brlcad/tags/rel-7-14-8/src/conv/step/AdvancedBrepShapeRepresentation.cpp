/*                 AdvancedBrepShapeRepresentation.cpp
 * BRL-CAD
 *
 * Copyright (c) 1994-2009 United States Government as represented by
 * the U.S. Army Research Laboratory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 */
/** @file AdvancedBrepShapeRepresentation.cpp
 *
 * Routines to convert STEP "AdvancedBrepShapeRepresentation" to BRL-CAD BREP
 * structures.
 *
 */
#include "AdvancedBrepShapeRepresentation.h"

AdvancedBrepShapeRepresentation::AdvancedBrepShapeRepresentation() {
	// TODO Auto-generated constructor stub

}

AdvancedBrepShapeRepresentation::~AdvancedBrepShapeRepresentation() {
	// TODO Auto-generated destructor stub
}
