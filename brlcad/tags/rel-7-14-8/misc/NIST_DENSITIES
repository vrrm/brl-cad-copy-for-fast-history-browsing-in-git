#***************************************************************************************
#
#                           BRL-CAD NIST Density Table v 1.0
#
#     *** IMPORTANT:  This file may change over time and should not be relied upon
#     *** for production analysis work.
#
#     Based on data present in various databases available at http://www.nist.gov - each
# subsection below cites the particular database from which information is drawn.  
#
#     The numbering provided for each entry is somewhat arbitrary in that the number
# does not typically contain any information about the particular material in question,
# but each "grouping" of materials will share a common two digit prefix.  For example,
# all pure elements will share the prefix "11".
#
#     All densities are in units of g/cm^3.
#
#***************************************************************************************


#			  Densities of Elementally Pure Materials
#
#     Drawn from the FCOMP database file included with ESTAR, available from this web
# address:  http://physics.nist.gov/PhysRefData/Star/Text/contents.html
#
# Numbering Conventions:  All pure elements will  have a prefix of "11".  The last three
# digits will be the atomic number of the element in question.  The third digit will be
# the beginning of a counter - the first value will always be one, and any subsequent
# entries for the same element will be counted up.

111001	0.000083748	Hydrogen
111002	0.000166322	Helium
111003	0.534000	Lithium
111004	1.84800		Beryllium
111005	2.37000		Boron
111006	2.00000		Carbon, Amorphous
112006	1.70000		Carbon, Graphite
111007	0.00116528	Nitrogen
111008	0.00133151	Oxygen
111009	0.00158029	Fluorine
111010	0.000838505	Neon
111011	0.97100		Sodium
111012	1.74000		Magnesium
111013	2.69890		Aluminum
111014	2.33000		Silicon
111015	2.20000		Phosphorus
111016	2.00000		Sulfur
111017	0.00299473	Chlorine
111018	0.00166201	Argon
111019	0.862000	Potassium
111020	1.55000		Calcium
111021	2.98900		Scandium
111022	4.54000		Titanium
111023	6.11000		Vanadium
111024	7.18000		Chromium
111025	7.44000		Manganese
111026	7.87400		Iron
111027	8.90000		Cobalt
111028	8.90200		Nickel
111029	8.96000		Copper
111030	7.13300		Zinc
111031	5.90400		Gallium
111032	5.32300		Germanium
111033	5.73000		Arsenic
111034	4.50000		Selenium
111035	0.00707218	Bromine
111036	0.00347832	Krypton
111037	1.53200		Rubidium
111038	2.54000		Strontium
111039	4.46900		Yttrium
111040	6.50600		Zirconium
111041	8.57000		Niobium
111042	10.2200		Molybdenum
111043	11.5000		Technetium
111044	12.4100		Ruthenium
111045	12.4100		Rhodium
111046	12.0200		Palladium
111047	10.5000		Silver
111048	8.65000		Cadmium
111049	7.31000		Indium
111050	7.31000		Tin
111051	6.69100		Antimony
111052	6.24000		Tellurium
111053	4.93000		Iodine
111054	0.00548536	Xenon
111055	1.87300		Cesium
111056	3.50000		Barium
111057	6.15400		Lanthanum
111058	6.65700		Cerium
111059	6.71000		Praseodymium
111060	6.90000		Neodymium
111061	7.22000		Promethium
111062	7.46000		Samarium
111063	5.24300		Europium
111064	7.90040		Gadolinium
111065	8.22900		Terbium
111066	8.55000		Dysporosium
111067	8.79500		Holmium
111068	9.06600		Erbium
111069	9.32100		Thulium
111070	6.73000		Ytterbium
111071	9.84000		Lutetium
111072	13.3100		Hafnium
111073	16.6540		Tantalum
111074	19.3000		Tungsten
111075	21.0200		Rhenium
111076	22.5700		Osmium
111077	22.4200		Iridium
111078	21.4500		Platinum
111079	19.3200		Gold
111080	13.5460		Mercury
111081	11.7200		Thallium
111082	11.3500		Lead
111083	9.74700		Bismuth
111084	9.32000		Polonium
111085	9.32000		Astatine
111086	0.00906618	Radon
111087	1.00000		Francium
111088	5.00000		Radium
111089	10.0700		Actinium
111090	11.7200		Thorium
111091	15.3700		Protactinium
111092	18.9500		Uranium
111093	20.2500		Neptunium
111094	19.8400		Plutonium
111095	13.6700		Americium
111096	13.5100		Curium
111097	14.0000		Berkelium
111098	10.0000		Californium

#			     Densities of Real World Materials
#
#     Drawn from the FCOMP database file included with ESTAR, available from this web
# address:  http://physics.nist.gov/PhysRefData/Star/Text/contents.html
#
# Numbering Conventions:  All real world materials from FCOMP will have a prefix of
# 12, followed by their ID number in the FCOMP database.

12099	1.12700		A-150 Tissue-Equivalent Plastic
12100	0.78990		Acetone 
12101	0.00109670	Acetylene 
12102	1.35000		Adenine 
12103	9.20000		Adipose Tissue (ICRP)
12104	0.00120479	Air, Dry (near sea level)
12105	1.42000		Alanine
12106	3.97000		Aluminum Oxide
12107	1.10000		Amber
12108	0.000826019	Ammonia
12109	1.02350		Aniline
12110	1.28300		Anthracene
12111	1.45000		B-100 Bone-Equivalent Plastic
12112	1.25000		Bakelite
12113	4.89000		Barium Fluoride 
12114	4.50000		Barium Sulfate
12115	0.878650	Benzene
12116	3.01000		Beryllium oxide
12117	7.13000		Bismuth Germanium oxide
12118	1.06000		Blood (ICRP)
12119	1.85000		Bone, Compact (ICRU)
12120	1.85000		Bone, Cortical (ICRP)
12121	2.52000		Boron Carbide
12122	1.81200		Boron Oxide
12123	1.03000		Brain (ICRP)
12124	0.00249343	Butane
12125	0.809800	N-Butyl Alcohol
12126	1.76000		C-552 Air-Equivalent Plastic
12127	6.20000		Cadmium Telluride
12128	7.90000		Cadmium Tungstate
12129	2.80000		Calcium Carbonate
12130	3.18000		Calcium Fluoride
12131	3.30000		Calcium Oxide
12132	2.96000		Calcium Sulfate
12133	6.06200		Calcium Tungstate
12134	0.00184212	Carbon Dioxide
12135	1.59400		Carbon Tetrachloride
12136	1.42000		Cellulose Acetate, Cellophane
12137	1.20000		Cellulose Acetate Butyrate
12138	1.49000		Cellulose Nitrate
12139	1.03000		Ceric Sulfate Dosimeter Solution
12140	4.11500		Cesium Fluoride
12141	4.51000		Cesium Iodide 
12142	1.10580		Chlorobenzene
12143	1.48320		Chloroform
12144	2.30000		Concrete, Portland
12145	0.779000	Cyclohexane
12146	1.30480		1,2-Ddihlorobenzene 
12147	1.21990		Dichlorodiethyl Ether 
12148	1.23510		1,2-Dichloroethane
12149	0.713780	Diethyl Ether 
12150	0.948700	N,N-Dimethyl Formamide 
12151	1.10140		Dimethyl Sulfoxide 
12152	0.00125324	Ethane
12153	0.789300	Ethyl Alcohol
12154	1.13000		Ethyl Cellulose
12155	0.00117497	Ethylene
12156	1.10000		Eye Lens (ICRP)
12157	5.20000		Ferric Oxide
12158	7.15000		Ferroboride
12159	5.70000		Ferrous Oxide 
12160	1.02400		Ferrous Sulfate Dosimeter Solution 
12161	1.12000		Freon-12 
12162	1.80000		Freon-12B2 
12163	0.95000		Freon-13 
12164	1.50000		Freon-13B1
12165	1.80000		Freon-13I1 
12166	7.44000		Gadolinium Oxysulfide
12167	5.31000		Gallium Arsenide 
12168	1.29140		Gel in Photographic Emulsion
12169	2.23000		Glass, Pyrex
12170	6.22000		Glass, Lead 
12171	2.40000		Glass, Plate
12172	1.54000		Glucose
12173	1.46000		Glutamine 
12174	1.26130		Glycerol
12175	1.58000		Guanine
12176	2.32000		Gypsum, Plaster of Paris
12177	0.683760	N-Heptane 
12178	0.660300	N-Hexane
12179	1.42000		Kapton Polyimide Film 
12180	6.28000		Lanthanum Oxybromide 
12181	5.86000		Lanthanum Oxysulfide 
12182	9.53000		Lead Oxide
12183	1.17800		Lithium Amide 
12184	2.11000		Lithium Carbonate 
12185	2.63500		Lithium Fluoride
12186	0.820000	Lithium Hydride 
12187	3.49400		Lithium Iodide
12188	2.01300		Lithium Oxide
12189	2.44000		Lithium Tetraborate
12190	1.05000		Lung (ICRP)
12191	1.05000		M3 Wax
12192	2.95800		Magnesium Carbonate
12193	3.00000		Magnesium Fluoride
12194	3.58000		Magnesium Oxide
12195	2.53000		Magnesium Tetraborate
12196	6.36000		Mercuric Iodide
12197	0.000667151	Methane
12198	0.791400	Methanol
12199	0.990000	Mix D Wax
12200	1.000000	MS20 Tissue Substitute
12201	1.04000		Muscle, Skeletal
12202	1.04000		Muscle, Striated
12203	1.11000		Muscle-Equivalent Liquid, with Sucrose
12204	1.07000		Muscle-Equivalent Liquid, without Sucrose
12205	1.14500		Naphthalene
12206	1.19867		Nitrobenzene
12207	0.00183094	Nitrous Oxide
12208	1.08000		Nylon, Du Pont ELVAmide 8062
12209	1.14000		Nylon, type 6 and type 6/6
12210	1.14000		Nylon, type 6/10
12211	1.42500		Nylon, type 11 (Rilsan)
12212	0.702600	Octane, Liquid 
12213	0.930000	Paraffin Wax 
12214	0.626200	N-Pentane 
12215	3.81500		Photographic Emulsion 
12216	1.03200		Plastic Scintillator (Vinyltoluene based)
12217	1.14600		Plutonium Dioxide 
12218	1.17000		Polyacrylonitrile 
12219	1.20000		Polycarbonate (Makrolon, Lexan)
12220	1.30000		Polychlorostyrene 
12221	0.940000	Polyethylene 
12222	1.40000		Polyethylene Terephthalate (Mylar)
12223	1.19000		Polymethyl Methacralate (Lucite, Perspex)
12224	1.42500		Polyoxymethylene
12225	0.900000	Polypropylene
12226	1.06000		Polystyrene
12227	2.20000		Polytetrafluoroethylene (Teflon) 
12228	2.10000		Polytrifluorochloroethylene
12229	1.19000		Polyvinyl Acetate
12230	1.30000		Polyvinyl Alcohol 
12231	1.12000		Polyvinyl Butyral
12232	1.30000		Polyvinyl Chloride 
12233	1.70000		Polyvinylidene Chloride, Saran
12234	1.76000		Polyvinylidene Fluoride 
12235	1.25000		Polyvinyl Pyrrolidone
12236	3.13000		Potassium Iodide 
12237	2.32000		Potassium Oxide 
12238	0.00187939	Propane
12239	0.430000	Propane, Liquid 
12240	0.803500	N-Propyl Alcohol
12241	0.981900	Pyridine
12242	0.920000	Rubber, Butyl 
12243	0.920000	Rubber, Natural 
12244	1.23000		Rubber, Neoprene
12245	2.32000		Silicon Dioxide 
12246	6.47300		Silver Bromide 
12247	5.56000		Silver Chloride 
12248	6.47000		Silver Halides in Photographic Emulsion 
12249	6.01000		Silver Iodide 
12250	1.10000		Skin (ICRP) 
12251	2.53200		Sodium Carbonate 
12252	3.66700		Sodium Iodide 
12253	2.27000		Sodium Monoxide 
12254	2.26100		Sodium Nitrate 
12255	0.970700	Stilbene
12256	1.58050		Sucrose
12257	1.23400		Terphenyl 
12258	1.04000		Testes (ICRP) 
12259	1.62500		Tetrachloroethylene
12260	7.00400		Thallium Chloride 
12261	1.00000		Tissue, Soft (ICRP)
12262	1.00000		Tissue, Soft (ICRU four-component)
12263	0.00106409	Tissue-Equivalent GAS (Methane based) 
12264	0.00182628	Tissue-Equivalent GAS (Propane based) 
12265	4.26000		Titanium Dioxide 
12266	0.866900	Toluene
12267	1.46000		Trichloroethylene 
12268	1.07000		Triethyl Phosphate
12269	2.40000		Tungsten Hexafluoride 
12270	11.2800		Uranium Dicarbide 
12271	13.6300		Uranium Monocarbide
12272	10.9600		Uranium Oxide
12273	1.32300		Urea
12274	1.23000		Valine
12275	1.80000		Viton Fluoroelastomer
12276	1.00000		Water, Liquid 
12277	0.000756182	Water Vapor
12278	0.870000	Xylene
