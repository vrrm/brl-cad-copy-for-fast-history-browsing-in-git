BRL-CAD on Max OS X README
==========================

Being that this is one of the newest architectures to be added and
officially supported, there are some issues to keep in mind with the
installation when building from the sources.  Beyond the notes
provided here, building on Mac OS X can generally be considered the
same as the UNIX, BSD, and Linux platforms.

Table of Contents
-----------------
  Introduction
  Table of Contents
  X11 Window Server
  Supported Versions
  Mac OS X 10.2
  Parallel Builds
  Bugs


X11 Window Server
-----------------
You'll need to install an X11 server if you would like to build Tk and
have a graphical user interface.  Apple's X11 is the recommended X
Window Server, though XDarwin should work suitably as well.  Be sure
to install the developer headers and libraries from the X11 Software
Developers Kit (SDK), which is usually a separate installation
package.

Supported Versions
------------------
BRL-CAD is generally only "extensively" tested by the developers on
the latest released version of Mac OS X only.  That is to say that
although the BRL-CAD package should build on prior versions of the Mac
OS X operating system, they are generally and eventually not
maintained and will require additional effort to obtain a build.
Compiling on 10.3 or 10.4 should complete successfully.  Due to a
variety of significant application programming interface issues in
early releases of Mac OS X, versions prior to 10.2 are unsupported.

Mac OS X 10.2
-------------
Compiling directly on Mac OS X 10.2 requires additional effort.  When
compiling from source, a libtool script is generated after running
autogen.sh that gets used during compilation.  The version of
glibtoolize shipped with 10.2 is non-functional with respect to
convenience libraries, a feature that BRL-CAD requires.  That means
that you will need to obtain the generated libtool script from other
means.  You can either run autogen.sh on a different machine that has
a newer version of libtool installed or you can download a source
disribution where autogen.sh has already been run for you on a
relatively new system.

Parallel Builds
---------------
As many workstation and server systems shipped by Apple are
multiprocessor systems, you can enjoy the benefits of decreased
compile times by utilizing the "-j" option to make.  After running
configure, run "make -j2" to build on a 2-processor system.

Universal Builds 
----------------

Universal builds have not been fully vetted due to how BRL-CAD
serializes data to disk.  It is possible to make a universal build,
but whether the build behaves correctly has not yet been validated and
as such is not recommended.

CMake and Mac OSX
-----------------

On a default configuration, CMake has some problems performing parallel
builds on OSX - typically this will manifest itself as a series of 
"too many open files" errors.  So far, the way to work around this in
OSX 10.5 is to set the following system control variables to 50,000:

kern.maxfiles
kern.maxfilesperproc

(check the current values using "sysctl <variable>" on the command line)

and up the maxfiles limit in /etc/launchd.conf:

limit maxfiles 50000 unlimited

This is not widely tested, and OSX has quite a few limit settings that
may or may not apply (and may change from one version to the next.)  As
we get a better feel for various platforms we can assemble a table of
required settings.  Other programs have this issue as well, so searching
online may prove fruitful.

So far, if parallel building doesn't work due to the above issue a 
non-parallel build will still succeed.  Another workaround is to build
just subdirectories (e.g. src/other, doc, etc.) individually and then
do a toplevel make in non-parallel mode to finalize the build.


64-bit Compilation
------------------

TODO - verify proper behavior on the CMake build for 64 bit OSX.

Compiling 64-bit on Mac OS X is possible but will not occur by merely
specifying the --enable-64-bit configure flag.  You must specify
additional CFLAGS/CXXFLAGS/CPPFLAGS/LDFLAGS to get a 64-bit
compilation.  The exact values for these flags depends on environment
and is beyond the scope of this document -- if you don't know what
settings you need, you probably shouldn't be compiling.  That said,
this just might work for you:

./configure CFLAGS=-m64 CXXFLAGS=-m64 LDFLAGS=-m64 --enable-64bit

Tips
----

If you need to get information about your machine and OSX version, try
these commands:

sw_vers -productVersion
uname -a

Bugs
----
The only known bugs specific to Mac OS X are limitations of Tk or the
X11 event handler that our generally outside of BRL-CAD's domain.
Refer to BUGS for more general details on known bugs and reporting
mechanisms.

Cheers!
