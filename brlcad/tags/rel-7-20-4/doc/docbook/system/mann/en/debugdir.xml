<!-- Converted by db4-upgrade version 1.0 -->

<refentry xmlns="http://docbook.org/ns/docbook" version="5.0" xml:id="debugdir">
  
  <refmeta>
    <refentrytitle>DEBUGDIR</refentrytitle>
    <manvolnum>nged</manvolnum>
    <refmiscinfo class="source">BRL-CAD</refmiscinfo>
    <refmiscinfo class="manual">BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv xml:id="name">
    <refname>debugdir</refname>
    <refpurpose>
      Displays a dump of the in-memory directory for the current
      database file.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv xml:id="synopsis">
    <cmdsynopsis sepchar=" ">
      <command>debugdir</command>    
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsection xml:id="description"><info><title>DESCRIPTION</title></info>
    
    <para>
      Displays a dump of the in-memory directory for the current
      database file. The information listed for each directory entry includes:
    </para>
    <para>
      <itemizedlist mark="bullet">
	<listitem>
	  <para>memory address of the directory structure.</para>
	</listitem>
	<listitem>
	  <para>name of the object.</para>
	</listitem>
	<listitem>
	  <para>"d_addr" for objects on disk, or "ptr" for objects in memory.</para>
	</listitem>
	<listitem>
	  <para>
	    "SOL," "REG," or "COM" if the object is a shape, region, or 
	    combination, respectively.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    file offset (for objects on disk) or memory pointer 
	    (for objects in memory).
	  </para>
	</listitem>
	<listitem>
	  <para>
	    number of instances referencing this object (not normally filled in).
	  </para>
	</listitem>
	<listitem>
	  <para>number of database granules used by this object.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    number of times this object is used as a member in combinations 
	    (not normally filled in).
	  </para>
	</listitem>
      </itemizedlist> 
    </para>
  </refsection>
  
  <refsection xml:id="examples"><info><title>EXAMPLES</title></info>
    
    <para>
      The example shows the use of the <command>debugdir</command> command to 
      get a dump of the in-memory directory.
    </para>
    <example><info><title>Get a dump of the in-memory directory.</title></info>
      
      <para>
	<prompt>mged&gt;</prompt> <userinput>debugdir</userinput>
      </para>
      <para>Gets a dump of the in-memory directory.</para>
    </example>
  </refsection>
  
  <info><corpauthor>BRL-CAD Team</corpauthor></info>
  
  <refsection xml:id="bug_reports"><info><title>BUG REPORTS</title></info>
    
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsection>
</refentry>
