package BRLCAD_DB_VALIDATION;

# path to xmllint executable
our $XMLLINT = '/usr/bin/xmllint';

# path to Multi-Schema Validator (MSV) jar file
our $MSVJAR = '/opt/oracle/msv-20090415/msv.jar';

# path to Java executable
our $JAVA = '/opt/sun/jre1.6.0_21/bin/java';

# path to oNVDL jar file
our $ONVDLJAR = '/opt/oNVDL/bin/onvdl.jar';
