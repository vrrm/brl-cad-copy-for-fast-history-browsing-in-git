This directory holds the DocBook (DB) documentation for BRL-CAD. Over
time, this should become the repository for all tutorials, man pages,
and other non-autogenerated, formatted documentation for BRL-CAD.

The xsl stylesheets for docbook are presently in the resources directory.

The current structure is as follows:

articles/
	Individual articles on specific topics - oed and tire live here,
	as well as the articles that made up the appendices of Vol III.

books/
	Either compilations of smaller documents into large works, or
	individual self contained works.  Most of Volume III is in a
	single file in this subtree.

lessons/
	Individual documents intended to teach some aspect of BRL-CAD
	to new users/students.  At this time it contains the individual
	lessons that made up Volume II.

resources/
	The DocBook xsl files are contained within the "standard"
	subdirectory.  Custom files will be organized under this
	directory in the future. Also included are various directories
	and files associated with DB validation, fonts, hyphenation,
	BRL-CAD common files and images, etc.

system/
	Documentation similar to traditional Unix "man" pages - focused
	documentation on specific parts of BRL-CAD.  There is a template
        for authors of mged command man pages at:

          ./system/mann/mged_cmd_template.xml

presentations/
	Copies of briefings (slides or slides converted to documents,
	e.g., "Introduction to Tcl/Tk."


LANGUAGE CONSIDERATIONS
-----------------------

English documents are in 'en' sub-directories.  Other languages are
found in standard two-character sub-directories.  See, for example,
the Spanish translations in 'lessons/es'.  Currently, the following
language codes may be found in various sub-directories:

  * en - English
  * es - Spanish
  * it - Italian
  * ru - Russian

See <http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes> for one
listing of ISO language codes.

English DocBook source files are expected to be ASCII with xhtml
unicode markup for non-ASCII symbols.  Other language source files
should be in UTF-8.

Notes on equation generation in svg, lists of common symbols and their
unicode DB symbols, and other notes on DB authoring tips are found in
the file 'README.DB_authors_notes'.

The text character code requirements may eventually be enforced by a
pre-commit hook script.


DOCBOOK PROCESSING
------------------

Currently there are three products generated from DB source:  

  * man pages

  * html documents

  * pdf documents

There are some external tools required in order to produce those
documents:

  For all DB documents (man, html, and pdf):

    * xsltproc
        a program that comes with libxslt (see http://xmlsoft.org/XSLT/)

    * saxon-he
        an Open Source java xslt processor (capable of using XSLT 2.0
        and the new DB XSLT 2.0 stylesheets released on 2011-08-25
        [not yet used or provided with BRL-CAD]) (see
        http://saxon.sourceforge.net/)

  For pdf documents:

    * fop
        a java program provide by the Apache Software Foundation; note
        that a minimum of version 1.0 is required to use the
        hyphenation binaries provided in thre resources sub-directory
        (see http://xmlgraphics.apache.org/fop/).  Also, version 1.0
        has some new font support features and we recommend using that
        version (or later) for DB processing.

    * Java
        a working Java run-time environment

    * scribus
        an Open Source page layout program; use to convert eps to svg
        (see http://www.scribus.net)

  For checking the well-formedness of any xml document:

    * xmllint
        a program that comes with libxml2 (see http://xmlsoft.org/)

  For creating and editing svg equations:

    * LibreOffice
        a free office suite whose text editor can be used to create
        and edit equations and export them to MathML (mml) files (see
        http://www.libreoffice.org/)

    * Inkscape
        a powerful and free vector drawing editor (see
        http://inkscape.org)

    * math2svg 
        a program (from the SVGMath suite) to convert MathML xml
        format to SVG xml format (see
        http://http://grigoriev.ru/svgmath/ and
        http://sourceforge.net/projects/svgmath/)

        Note that the output sometimes needs further editing either
        manually or by inkscape.

See this link for help in installing fop 1.0 and saxon-he on Ubuntu:

  http://johnbokma.com/mexit/2011/07/04/

VALIDATION
----------

In this directory there is a set of experimental tools to assist
developers and DB authors in ensuring the DB files are valid:

  * validate.pl
      a Perl program to drive validation of DB source files

  * BRLCAD_DB_VALIDATION.pm
      a Perl module defining local tool paths; used by validate.pl;
      intended for DB authors to edit for their use; valid tool paths
      must be entered for each validation tool to be used; see the
      file for more information on the paths needed

  * find-db-files.pl
      a Perl program to create an input file listing all DB source
      files in recognized DB sub-directories

In order to use the validation program the user must obtain some
external tools that are not presently part of the BRL-CAD package:

  For the 'msv' validation method (the default and recommended method):

    * msv 
        Oracle Multi-Schema XML Validator (MSV) (see http://msv.java.net/)

    * Java
        a working Java run-time environment

  For the 'xmllint' validation method:

    * xmllint
        a program that comes with the libxml2 library (see
        http://xmlsoft.org/)

  For the 'nvdl' validation method:

    * onvdl.jar
        a Java jar file with the oNVDL validation tool (see 
        https://sourceforge.net/projects/onvdl/

    * Java
        a working Java run-time environment

     
OTHER CONSIDERATIONS
--------------------

These documents make extensive use of the XInclude modular
documentation specification. Consequently, the contents of any one
particular "document" may be scattered over many other documents.
This design direction is intended to allow re-use of standard
definitions and descriptions in multiple documents serving multiple
purposes.  When a definition is updated for one document,
re-generation of other documents will incorporate the update as well
without requiring redundant editing.

Caution - the BRL-CAD build logic is not aware of the interlinking of
various xml files using xinclude.  This means that a change on one document
may make other output files out of date, and this will not trigger a
re-build of those files automatically.

Notes:

* When rendered to html output and placed on a server, there may arise
  a problem where the server supplies the page as ISO-8859-1 encoded
  despite them actually using UTF-8.  To combat this problem in the
  case of the Apache web server, an .htaccess file in the directory 
  with the line:

    AddDefaultCharset UTF-8

  should let Apache know to use UTF-8 encoding for these pages.
