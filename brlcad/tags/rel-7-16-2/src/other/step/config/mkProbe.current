#!/bin/csh
###############################################################################
# To create a new Data Probe use the mkProbe script as follows:
#
# mkProbe [-i] [-l] [-p] [-s] [-R pdes_root] [-E express_path] 
#         express-file schema_name
#

# mkProbe creates a new Data Probe executable using the express-file
# argument.  The new Data Probe has the name dp_schema-name and is
# located in a (possibly new) directory named schema-name.

# The optional argument -i indicates that the new data probe should be
# installed into the PDES root directory.  Without the -i argument the
# new Data Probe will be created in the directory from which the
# mkProbe script is run.

# The optional argument -l tells the script to only create the library
# for the schema and not to create the Data Probe application.

# The optional argument -p tells the script to only create the Part 21
# file reader application rather than the Data Probe.

# The optional argument -E specifies an EXPRESS_PATH environment
# variable to be used when fedex_plus is executed in mkProbe.  (See
# documentation on the NIST EXPRESS Toolkit.)  If this option is not
# chosen, mkProbe will use the EXPRESS_PATH environment variable set
# in the user's .cshrc file or if EXPRESS_PATH is not set in the
# .cshrc file, mkProbe will use the EXPRESS_PATH setting it finds when
# executed (i.e., from a previous setenv EXPRESS_PATH command or the
# user's .login file).

# The optional argument -s (or -S) tells the EXPRESS-to-C++
# translator, fedex_plus, to use only single-inheritance, rather than
# multiple inheritance.  This option is just passed along to fedex_plus.

# The PDES root directory can be defined by the environment variable
# PDES_ROOT.  This variable should be set in the user's .cshrc file as
# follows:

#
#	% setenv PDES_ROOT = ~pdes
#

# If the PDES_ROOT environment variable is not set mkProbe looks for
# the directory ~pdevel and uses that as the PDES root.
#
###############################################################################

#####
# -----	set up variables and directories

#set USAGE = "usage:  mkProbe [-i] [-l] [-p] [-s] [-R pdes_root] [-E express_path] express-file schema_name"
set origDir = `pwd`

#to flag the -i option when chosen
set useroot = 'no'
#

# set up a flags variable for fedex_plus
set fedXflags = ''

# --  find the root directory
# This can be overridden with the -R option to mkProbe option
if ($?PDES_ROOT) then
set pdesRoot=$PDES_ROOT
else 
set pdesRoot = '/proj/pdevel/scl3-1'
endif

#####
# --  set up to install into a public place
# and other options

set mkrule = ''

foreach i ($1 $2 $3 $4 $5 $6)
 switch ($i)
 case -i: 
 case -I: 
#  set up the installation locations last
	set useroot = '-i';
     	shift;
     	breaksw
 case -s:
 case -S:
# set up flag to fedex_plus for single-inheritance
	set fedXflags = $i;
	shift;
	breaksw
 case -e:
 case -E:
# set the EXPRESS_PATH for fed-x to use
     setenv EXPRESS_PATH $2;
     shift;
     shift;
     breaksw
 case -r:
 case -R:
# set the PDES_ROOT
     set pdesRoot = $2;
     shift;
     shift;
     breaksw
 case -L:
 case -l:
# create only the schema library and not the Data Probe application
     set mkrule = 'lib';
     shift;
     breaksw
 case -p:
 case -P:
# create the Part 21 file reader and not the Data Probe application
     set mkrule = 'p21';
     shift;
     breaksw
 default:
# don't shift so that the express file and schema name will be left
     breaksw
 endsw

end

if ( -d $pdesRoot ) then
else
echo "usage:  mkProbe [-i] [-l] [-p] [-s] [-R pdes_root] [-E express_path] express-file schema_name"
echo " -i install in the scl root directory"
echo " -l create only the library for the schema"
echo " -p create only the Part 21 file reader application"
echo " -s passed to fedex_plus to generate only single inheritance for entities"
echo "Use the environment variable PDES_ROOT or the -R option"
echo "to set the root of the pdes directory." 
exit(-1)
endif

# --  set arch directory
set archRoot = $pdesRoot/arch-gnu-solaris

# --  set fedex_plus
set fedXplus = $archRoot/bin/fedex_plus

#####
# --  set up directories for installation
set EXPRESS_FILE = $1
set SCHEMA_NAME = $2

if $useroot == '-i' then 
# --  set up to create files in public directory
set OFILES = $archRoot/Probes/$SCHEMA_NAME
set SRCS = $pdesRoot/src/clSchemas/$SCHEMA_NAME
set TMP_MAKE = $archRoot/Probes/template-schema/Makefile.public

else
# --  set up to create files in personal directory
set OFILES = $origDir/$SCHEMA_NAME
set SRCS = $origDir/$SCHEMA_NAME
set TMP_MAKE = $archRoot/Probes/template-schema/Makefile.personal
endif

#####
# --  make all directories
if ( -d $SRCS ) then
else
mkdir $SRCS
endif

if (-w $SRCS ) then 
else 
echo "Unable to write to directory $SRCS"
echo "usage:  mkProbe [-i] [-l] [-p] [-s] [-R pdes_root] [-E express_path] express-file schema_name"
echo " -i install in the scl root directory"
echo " -l create only the library for the schema"
echo " -p create only the Part 21 file reader application"
echo " -s passed to fedex_plus to generate only single inheritance for entities"
exit(-1)
endif

if ( -d $OFILES ) then
else
mkdir $OFILES
endif

if (-w $OFILES ) then 
else 
echo "Unable to write to directory $OFILES"
echo "usage:  mkProbe [-i] [-l] [-p] [-s] [-R pdes_root] [-E express_path] express-file schema_name"
echo " -i install in the scl root directory"
echo " -l create only the library for the schema"
echo " -p create only the Part 21 file reader application"
echo " -s passed to fedex_plus to generate only single inheritance for entities"
exit(-1)
endif

#####
# --  if public set up some links for convience

if ($useroot == '-i') then
# set up links only if they do not already exist
if ((-e $OFILES/src) || (-e $SRCS/o)) then
else
ln -s $OFILES $SRCS/o
ln -s $SRCS $OFILES/src
endif
endif

#####
# --  check the arguments

if ($EXPRESS_FILE == '') then
echo "No EXPRESS file supplied"
echo "usage:  mkProbe [-i] [-l] [-p] [-s] [-R pdes_root] [-E express_path] express-file schema_name"
echo " -i install in the scl root directory"
echo " -l create only the library for the schema"
echo " -p create only the Part 21 file reader application"
echo " -s passed to fedex_plus to generate only single inheritance for entities"
exit(-1)
endif 

if ( -r $EXPRESS_FILE) then
set expFile = `basename $EXPRESS_FILE`
set expressPath = `dirname $EXPRESS_FILE`
cd $expressPath
set fullExpPath = `pwd`
cd $SRCS
ln -s $fullExpPath/$expFile
cd $origDir
else 
echo "EXPRESS file is not readable"
echo "usage:  mkProbe [-i] [-l] [-p] [-s] [-R pdes_root] [-E express_path] express-file schema_name"
echo " -i install in the scl root directory"
echo " -l create only the library for the schema"
echo " -p create only the Part 21 file reader application"
echo " -s passed to fedex_plus to generate only single inheritance for entities"
exit(-1)
endif

if ($SCHEMA_NAME == '') then
echo "No schema name supplied"
echo "usage:  mkProbe [-i] [-l] [-p] [-s] [-R pdes_root] [-E express_path] express-file schema_name"
echo " -i install in the scl root directory"
echo " -l create only the library for the schema"
echo " -p create only the Part 21 file reader application"
echo " -s passed to fedex_plus to generate only single inheritance for entities"
exit(-1)
endif 

#####
# -----	everything's set, translate the Express file

echo " "
echo "Creating a Data Probe..."
echo " "
echo "EXPRESS file = $EXPRESS_FILE"
echo "schema name = $SCHEMA_NAME"
if ($?EXPRESS_PATH) then
echo "EXPRESS_PATH = $EXPRESS_PATH"
endif
echo "pdes root dir = $pdesRoot"
echo " "

cd $SRCS

# run fedex_plus on the Express schema to generate the C++ source code
echo "Translating the schema..."
# echo $fedXplus $fedXflags $fullExpPath/$expFile
$fedXplus $fedXflags $fullExpPath/$expFile

#####
# ----- source is generated, now compile it
cd $OFILES

# -- edit the makefile
if( -r Makefile ) then
mv Makefile Makefile.old
rm -f Makefile
endif
sed -e "s/SCHEMA_NAME = your-schema/SCHEMA_NAME = $SCHEMA_NAME/" $TMP_MAKE > Makefile
echo " "

#####
# -- compile the new probe
# no need to run make depend if not developing
#make PDES_ROOT=$pdesRoot depend

echo " "
echo "Compiling..."
make PDES_ROOT=$pdesRoot $mkrule

#####
# -- report what happened 
if ( -r libC$SCHEMA_NAME.a) then
echo " "
echo "Your schema library is libC$SCHEMA_NAME.a"

else 
echo "mkProbe was unsuccessful in creating the Schema Library."
echo " "
echo "Resolve previous error messages and try again." 
echo " "
exit (-1)
endif

# if only building the schema library stop here
if ($mkrule == 'lib') then

else if ( -x dp_$SCHEMA_NAME) then
echo " "
echo " "
echo "To run your new Data Probe..."
echo "run $OFILES/dp_$SCHEMA_NAME"

else if ( -x p21read_$SCHEMA_NAME) then
echo " "
echo " "
echo "Your Part 21 reader is in $OFILES/dp_$SCHEMA_NAME"

else
echo " "
echo " "
echo "mkProbe was unsuccessful in making the Data Probe."
echo " "
echo "Resolve previous error messages and try again." 
echo " "
exit (-1)
endif


