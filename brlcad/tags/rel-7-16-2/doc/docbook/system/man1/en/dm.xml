<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='dm'>
  
  <refmeta>
    <refentrytitle>DM</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>dm</refname>
    <refpurpose>
      Provides a means to interact with the display manager at a lower
      level.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>dm</command>    
      <arg choice='req'><replaceable>subcommand</replaceable></arg>
      <arg><replaceable>args</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      Provides a means to interact with the display manager at a lower
      level. The <command>dm</command> command accepts the following subcommands:
    </para>
    <variablelist>
      <varlistentry>
	<term><command>set</command> 
	    <group>   
	      <arg><replaceable>var</replaceable></arg>
	      <arg><replaceable>val</replaceable></arg>
	    </group>
	</term>
	<listitem>
	  <para>
	    The "set" subcommand provides a means to set or query display manager-specific
	    variables. Invoked without any arguments, the <emphasis>set</emphasis> subcommand 
	    will return a list of all available internal display manager variables. If only the 
	    <emphasis>var</emphasis> argument is specified, the value of that variable is 
	    returned. If both <emphasis>var</emphasis> and <emphasis>val</emphasis> are given, 
	    then <emphasis>var</emphasis> will be set to <emphasis>val</emphasis>.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><command>size</command><arg><replaceable>width height</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    The "size" subcommand provides a means to set or query the window size. If no
	    arguments are given, the display manager's window size is returned. If 
	    <emphasis>width</emphasis> and <emphasis>height</emphasis> are specified, the 
	    display manager makes a request to have its window resized. Note that a size 
	    request is just that, a request, so it may be ignored, especially if the user 
	    has resized the window using the mouse.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><command>m</command>
	    <arg choice='req'><replaceable>button</replaceable></arg>
	    <arg choice='req'><replaceable>x y</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    The "m" subcommand is used to simulate an <command>M</command> command. The 
	    <emphasis>button</emphasis> argument determines which mouse button is being 
	    used to trigger a call to this command. This value is used in the event 
	    handler to effect dragging the faceplate scrollbars. The <emphasis>x</emphasis> 
	    and <emphasis>y</emphasis> arguments are in X screen coordinates, which are 
	    converted to MGED screen coordinates before being passed to the 
	    <command>M</command> command.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>
	    <command>am</command>
	    <arg choice='req'> 
	      <group> 
		<arg>r</arg>
		<arg>t</arg>
		<arg>s</arg>
	      </group>
	    </arg>
	    <arg choice='req'><replaceable>xy</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    The "am" subcommand effects <emphasis>mged's</emphasis> alternate mouse mode. The 
	    alternate mouse mode gives the user a different way of manipulating the view or 
	    an object. For example, the user can drag an object or perhaps rotate the view while 
	    using the mouse. The first argument indicates the type of operation to perform 
	    (i.e., <emphasis>r</emphasis> for rotation, <emphasis>t</emphasis> for translation, 
	    and <emphasis>s</emphasis> for scale). The <emphasis>x</emphasis> and 
	    <emphasis>y</emphasis> arguments are in X screen coordinates and are transformed 
	    appropriately before being passed to the knob command.
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term>
	    <command>adc</command>
	    <arg choice='req'>
	      <group>
		<arg>1</arg>
		<arg>2</arg>
		<arg>t</arg>
		<arg>d</arg>
	      </group>
	    </arg> 
	    <arg choice='req'><replaceable>xy</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    The "adc" subcommand provides a way of manipulating the angle distance cursor
	    while using the mouse. The first argument indicates the type of operation to perform
	    (i.e., <emphasis>1</emphasis> for angle 1, <emphasis>2</emphasis> for angle 2, 
	    <emphasis>t</emphasis> for translate, and <emphasis>d</emphasis> for tick distance). 
	    The <emphasis>x</emphasis> and <emphasis>y</emphasis> arguments are in X screen 
	    coordinates and are transformed appropriately before being passed to the 
	    <command>adc</command> command (i.e., not "dm adc"). 
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>
	    <command>con</command>
	    <arg choice='req'>
	      <group>
		<group>
		  <arg>r</arg>
		  <arg>t</arg>
		  <arg>s</arg>
		</group>
		<group>     
		  <arg>x</arg>
		  <arg>y</arg>
		  <arg>z</arg>
		</group>
	      </group>
	    </arg>
	    <arg choice='req'><replaceable>xpos ypos</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    This form of the "con" subcommand provides a way to effect constrained
	    manipulation of the view or an object while using the mouse. This simulates the
	    behavior of sliders without taking up screen real estate. The first argument 
	    indicates the type of operation to perform (i.e., <emphasis>r</emphasis> for 
	    rotation, <emphasis>t</emphasis> for translation, and <emphasis>s</emphasis> for scale).
	    The &lt;<emphasis>x</emphasis>|<emphasis>y</emphasis>|<emphasis>z</emphasis>&gt; 
	    argument is the axis of rotation, translation, or scale. The <emphasis>xpos</emphasis> 
	    and <emphasis>ypos</emphasis> arguments are in X screen coordinates and are 
	    transformed appropriately before being passed to the knob command.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>
	    <command>con</command>
	    <arg choice='req'>a</arg>
	    <arg choice='req'>
	      <group>
		<arg>x</arg>
		<arg>y</arg>
		<arg>1</arg>
		<arg>2</arg>
		<arg>d</arg>
	      </group>
	    </arg>
	    <arg choice='req'><replaceable>xpos ypos</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    This form of the "con" subcommand provides a way to effect constrained
	    manipulation of the angle distance cursor while using the mouse. This simulates the
	    behavior of sliders without taking up screen real estate. The first argument indicates
	    that this is to be applied to the angle distance cursor. The next argument indicates the
	    type of operation to perform (i.e., <emphasis>x</emphasis> for translate in the 
	    <emphasis>x</emphasis> direction, <emphasis>y</emphasis> for translate in
	    the <emphasis>y</emphasis> direction, <emphasis>1</emphasis> for angle 1, 
	    <emphasis>2</emphasis> for angle 2, and <emphasis>d</emphasis>for tick distance). 
	    The <emphasis>xpos</emphasis> and <emphasis>ypos</emphasis> arguments are in X screen 
	    coordinates and are transformed appropriately before being passed to the knob 
	    command.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>
	    <command>valid</command>
	    <arg choice='req'>
	      <group>
		<arg>ogl</arg>
		<arg>X</arg>
		<arg>wgl</arg>
		<arg>rtgl</arg>
		<arg>...</arg>
	      </group>
	    </arg>
	</term>
	<listitem>
	  <para>
	    The "valid" subcommand provides a way to determine if a particular type of
	    display manager (X Windows, OpenGL, etc.) is available - if the display manager
            type is supported the string denoting that type (X, ogl, etc.) is returned back,
            otherwise nothing is returned.
  	  </para>
	</listitem>
      </varlistentry>
 
    </variablelist>
  </refsect1>
   
    <refsect1 id='examples'>
      <title>EXAMPLES</title>
      <para>
	The examples show the use of the <command>dm</command> command with its various 
	subcommands as presented in the Description section.
      </para>
      <example>
	<title>List the available display manager internal variables.</title>
	<para><prompt>mged></prompt> <userinput>dm set</userinput></para>
	<para>Lists the available display manager internal variables.</para>
      </example>
      
      <example>
	<title>Turn on perspective projection in the display using 
	<emphasis>var</emphasis> and <emphasis>val</emphasis> arguments.</title>
	<para>
	  <prompt>mged></prompt><userinput>dm set perspective 1</userinput>
	</para>
	<para>Turns on perspective projection in the display.       
	</para>
      </example>
      
      <example>
	<title>Query the display manager's window size.
	</title>
	<para>
	  <prompt>mged></prompt><userinput>dm size</userinput>
	</para>
	<para>The display manager's window size is returned.
	</para>
      </example>
      
      <example>
	<title>Resize the display manager window.
	</title>
	<para>
	  <prompt>mged></prompt><userinput>dm size 900 900</userinput>
	</para>
	<para>The display manager window is resized to 900 x 900.
	</para>
      </example>
      
      <example>
	<title>Simulate a button2 press at specific screen coordinates.
	</title>
	<para>
      <prompt>mged></prompt><userinput>dm m 2 100 200</userinput>
	</para>
	<para>Simulates a button2 press at (100, 200) in X screen coordinates.</para>
      </example>
      
      <example>
	<title>Use the alternate mouse mode to rotate an image.
	</title>
	<para>
	  <prompt>mged></prompt><userinput>dm am r 400 100</userinput>
	</para>
	<para>Starts an alternate mouse mode rotation at (400, 100). </para>
      </example>
      
      <example>
	<title>Start a tick distance manipulation with the mouse.
	</title>
	<para>
	  <prompt>mged></prompt><userinput>dm adc d 300 200</userinput>
	</para>
	<para>Starts a tick distance manipulation at (300, 200).</para>
      </example>
      
      <example>
	<title>Start a constrained translation down the Z axis using the mouse.
	</title>
	<para>
	  <prompt>mged></prompt><userinput>dm con t z 200 200</userinput>
	</para>
	<para>Starts a constrained translation down the Z axis.
	</para>
      </example>
      
      <example>
	<title>Start a constrained tick distance manipulation using the mouse.
	</title>
	<para>
	  <prompt>mged></prompt><userinput>dm con a d 200 100</userinput>
	</para>
	<para>Starts a constrained tick distance manipulation. 
	</para>
      </example>
      
      <example>
	<title>End the mouse drag.
	</title>
	<para>
	  <prompt>mged></prompt><userinput>dm idle</userinput>
	</para>
	<para>Ends the drag.
	</para>
      </example>
      
    </refsect1>
    
    <refsect1 id='author'>
      <title>AUTHOR</title>
      <para>BRL-CAD Team</para>
    </refsect1>
    
    <refsect1 id='bug_reports'>
      <title>BUG REPORTS</title>
      <para>
	Reports of bugs or problems should be submitted via electronic
	mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
      </para>
    </refsect1>
  </refentry>

