<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2009 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='B'>
  
  <refmeta>
    <refentrytitle>B</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>B</refname>
    <refpurpose>
      Clears the <emphasis>mged</emphasis> display of any currently 
      displayed objects, then displays the list of objects provided in 
      the parameter list.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>B</command>
      <group>    
	<arg choice='opt'>-A -o</arg>
	<arg choice='plain' rep='repeat'><replaceable>attribute name</replaceable> {<replaceable>value</replaceable>}</arg>
      </group>
      <arg choice='opt'>-s</arg>
      <arg choice='opt'>-C#/#/#</arg>
      <arg choice='opt'>-R</arg>
      <arg choice='opt'><replaceable>objects</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      Clears the mged display of any currently displayed objects, then displays the list of
      objects provided in the parameter list. Equivalent to the <command>Z</command> command followed by the
      command <command>draw</command> <emphasis>&lt;objects&gt;</emphasis>. The <emphasis>-C</emphasis> option 
      provides the user a way to specify a color that overrides all other color specifications including 
      combination colors and region id-based colors. The <emphasis>-A </emphasis>and <emphasis>-o</emphasis> 
      options allow the user to select objects by attribute. The <emphasis>-s</emphasis> option specifies 
      that subtracted and intersected objects should be drawn with solid lines rather than dot-dash lines. 
      The -<I>R</I> option means do not automatically resize the view if no other objects are displayed.
      See the <command>draw</command> command for a detailed description of the options.
    </para>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      The following are run from the MGED command prompt.
    </para>
    <example>
      <title>Display a named object</title>
      <variablelist>
	<varlistentry>
	  <term><prompt>mged></prompt> <userinput>B some_object</userinput></term>
	  <listitem>
	    <para>
	      The display clears, and the object named <emphasis>some_object</emphasis> is displayed.
	    </para>
	  </listitem>
	</varlistentry>
      </variablelist>
    </example>
    
    <example>
      <title>Draw objects having an attribute with a value </title>
      <para>
	<prompt>mged></prompt><userinput>B -A -o Comment {First comment} Comment {Second comment}</userinput>
      </para>
      <para>
	The display clears, then draws objects that have a "Comment" attribute with a value of either 
	"First comment" or "Second comment."
      </para>
    </example>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>

