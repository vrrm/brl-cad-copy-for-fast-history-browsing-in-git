	The Pro/Engineer to BRL-CAD conversion is a two step process.
First, in Pro/Engineer, you must select "Interface", then "Export",
then "BRL-CAD" under the part or assembly menu. The "current object"
will be output to a file in an ascii format.  The default file name will
be the name of the current object with a ".brl" suffix (note that the
Pro/Toolkit software makes the name of the current object all capital
letters). You may provide your own file name, and you may provide a
maximum chord height error for facetization and an angle control value
for additional improvement on small radii curves. It is recommended that
the angle control value be left at zero to avoid interferences between
curved surfaces (see the Pro/E "Interface Guide" for details about
these parameters. The second step is to use "proe-g" to convert the
".brl" file to a ".g" file (see "man proe-g").

	To inform Pro/Engineer about the Pro/Toolkit code that produces
the ".brl" file, you need to create a "protk.dat" file as follows:

line 1:name proe-brl
line 2:startup spawn
line 3:exec_file 'full path to wherever you install the proe-brl executable'
line 4:text_dir 'full path to the "text" directory'
line 5:revision 18
line 6:end

	See "protk.dat" in this directory for an example. The exec_file
must inlcude the name of the executable file, and the text_dir must
include the name of the "text" directory. In the "text" directory
Pro/Engineer will look for subdirectories named "menus" and "fullhelp"
and for a file named "usermsg.txt". The "text" directory in this
directory should be used or at least copied verbatim to where you want
to use it.

	The "protk.dat" file must be located where Pro/Engineer will look for it.
Pro/Engineer will look in your current directory and in Pro/Engineer's "text"
directory (as well as a few other places).

	If the "protk.dat" file and the "proe-brl" executable are installed
properly, you will see a message "BRL-CAD Export option added to Pro/E version
XX.X, build YYWW" in the Pro/Engineer message window.


