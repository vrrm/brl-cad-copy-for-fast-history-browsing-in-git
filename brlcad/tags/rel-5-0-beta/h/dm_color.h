#ifndef SEEN_DM_COLOR
#define SEEN_DM_COLOR

extern unsigned long dm_get_pixel();
extern void dm_copy_default_colors();
extern void dm_allocate_color_cube();
#endif /* SEEN_DM_COLOR */
