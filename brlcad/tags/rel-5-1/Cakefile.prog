/*
 *			C A K E F I L E . P R O G
 *
 *  Like Cakefile.lib, this is the common part for compiling programs.
 *
 *  Authors -
 *	Michael John Muuss
 *	Bob Parker
 *  
 *  Source -
 *	The U. S. Army Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005-5068  USA
 *  
 *  Distribution Status -
 *	Public Domain, Distribution Unlimitied.
 *
 *  $Header: /usr/Backup/upgrade/brlcad/brlcad/Cakefile.prog,v 11.2 1995-08-10 20:15:29 gdurf Exp $
 */

/* Default rule */
everything&::	PRODUCTS

%.o:	../SRCDIR/%.c HDR_DEP		if exist ../SRCDIR/%.c
	@rm -f ./%.o
	CC CFLAGS -c ../SRCDIR/%.c

%.o:	../SRCDIR/%.f 			if exist ../SRCDIR/%.f
	@rm -f ./%.o
	FC FFLAGS -c ../SRCDIR/%.f

/* There is a conflict here:  When debugging, it's nice to
 * be able to examine the intermediate .c file that LEX and YACC produce.
 * When trying to port to a new platform, those leftovers can "trick"
 * the unwary.
 */

/* Non-ANSI compilers can't take tab-# */
%.o:    ../SRCDIR/%.l			if exist ../SRCDIR/%.l
	-rm -f ./%.c ./%.o
        LEX -t LEX_OPTS ../SRCDIR/%.l | sed -e 's/^[ 	]#/#/' > ./%.c
	if test `wc -l < ./%.c` -lt 2; then rm -f ./%.c; exit 1; fi
	CC CFLAGS -I. -c ./%.c

%.o:    ../SRCDIR/%.y			if exist ../SRCDIR/%.y
	-rm -f ./%.c ./%.h ./%.o
        YACC YACC_OPTS ../SRCDIR/%.y
	mv ./y.tab.c ./%.c
	CC CFLAGS -I. -c ./%.c
