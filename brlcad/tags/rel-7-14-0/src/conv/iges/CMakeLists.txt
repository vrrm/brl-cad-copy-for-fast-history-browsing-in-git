include_directories(
    ../../../include
    ../../other/libz
    ../../other/openNURBS
    ../../other/libregex
    ../../other/tcl/generic
)

add_definitions(
    -DBRLCAD_DLL
    -DHAVE_CONFIG_H
    -DBRLCADBUILD
    -DON_DLL_IMPORTS
)

add_executable(iges
    BrepHandler.cpp
    brlcad_brep.cpp
    n_iges.cpp
    nmain.cpp
)
target_link_libraries(iges
    BrlcadCore
    openNURBS
)

add_executable(iges-g
    add_face.c
    add_inner_shell.c
    add_loop.c
    b-spline.c
    block2.c
    brep.c
    brlabs.c
    check_names.c
    cone.c
    conv_drawings.c
    convassem.c
    convinst.c
    convsolids.c
    convsurf.c
    convtree.c
    cyl.c
    do_subfigs.c
    docolor.c
    ell.c
    evalxform.c
    extrudcirc.c
    extrudcon.c
    extrude.c
    findp.c
    freetree.c
    get_att.c
    get_cnurb_curve.c
    get_edge_list.c
    get_iges_vertex.c
    get_outer_shell.c
    get_vertex.c
    get_vertex_list.c
    getcurve.c
    iges_types.c
    main.c
    make_face.c
    make_nurb_face.c
    makedir.c
    makegroup.c
    makemembers.c
    matmult.c
    orient_loops.c
    planar_nurb.c
    read_att.c
    read_edge_list.c
    read_vertex_list.c
    readcnv.c
    readcols.c
    readdbl.c
    readflt.c
    readglobal.c
    readint.c
    readmatrix.c
    readname.c
    readrec.c
    readstart.c
    readstrg.c
    readtime.c
    readtree.c
    recsize.c
    revolve.c
    showtree.c
    sphere.c
    spline.c
    splinef.c
    stack.c
    summary.c
    tor.c
    trimsurf.c
    usage.c
    wedge.c
)
target_link_libraries(iges-g
    BrlcadCore
)

add_executable(g-iges
    g-iges.c
    iges.c
)
target_link_libraries(g-iges
    BrlcadCore
)
