<!-- Converted by db4-upgrade version 1.0 -->

<refentry xmlns="http://docbook.org/ns/docbook" version="5.0" xml:id="sca">

<refmeta>
  <refentrytitle>SCA</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class="source">BRL-CAD</refmiscinfo>
  <refmiscinfo class="manual">BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv xml:id="name">
  <refname>sca</refname>
  <refpurpose>Used to apply a scaling factor.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv xml:id="synopsis">
  <cmdsynopsis sepchar=" ">
    <command>sca</command>    
    <group choice="req" rep="norepeat">
     <arg choice="opt" rep="norepeat">sfactor</arg>
     <arg choice="opt" rep="norepeat">x-sfactor y-sfactor z-sfactor</arg>
    </group>
  </cmdsynopsis>
</refsynopsisdiv>

<refsection xml:id="description"><info><title>DESCRIPTION</title></info>
  
  <para>
    Applies a scaling factor to an object or view. When passed one parameter,
    an "object or view" is applied a uniform scale factor. When passed three
    parameters, an "object" is applied a unique scale factor to each axis. The
    effect is determined by the Transform option in the Settings menu. This is
    normally affected by the current mode of operation in MGED (e.g., matrix
    edit, primitive edit, or viewing).
  </para>

  <para>
    NOTE: Three parameters can only be applied in "matrix edit" mode. Only
    scale pipe and tori primitives uniformly otherwise raytracer and other
    errors may occur.
  </para>
</refsection>

<refsection xml:id="examples"><info><title>EXAMPLES</title></info>
  
  <para>
    The example shows the use of the <command>sca</command> command to apply a
    scaling factor to an object or view.
  </para>
  <example><info><title>Apply a scaling factor to an object or view.</title></info>
    
    <para>
      <prompt>mged&gt;</prompt><userinput>sca 2</userinput>
    </para>
    <para>
      In matrix edit mode, the object being affected will get twice as big. In
      view mode, the size of the view will be doubled (showing twice the volume
      of space, hence making objects appear half their previous size on the
      display).
    </para>
  </example>

  <example><info><title>Apply a scaling factor to an object.</title></info>
    
    <para>
      <prompt>mged&gt;</prompt><userinput>sca 2 1 1</userinput>
    </para>
    <para>
      In matrix edit mode, the object being affected will get twice as big
      along the x-axis.
    </para>
  </example>
</refsection>

<info><corpauthor>BRL-CAD Team</corpauthor></info>

<refsection xml:id="bug_reports"><info><title>BUG REPORTS</title></info>
  
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsection>
</refentry>
