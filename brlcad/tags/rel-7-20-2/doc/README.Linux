BRL-CAD on Linux README
=======================

Below are installation and platform notes of relevance to particular
Linux distributions.

Table of Contents
-----------------
64-bit Compile
32-bit Compile
Arch Linux
Ubuntu/Debian
PPC64 Linux


64-bit Compile (on a platform that defaults to 32-bit)
--------------

rm -rf *cache*   # remove any previous configure results
./configure --prefix=/usr/brlcad/rel-MAJOR.MINOR.PATCH --enable-all --enable-64bit-build CFLAGS=-m64 CXXFLAGS=-m64 LDFLAGS=-m64


32-bit Compile (on a platform that defaults to 64-bit)
--------------

rm -rf *cache*   # remove any previous configure results
./configure --prefix=/usr/brlcad/rel-MAJOR.MINOR.PATCH --enable-all --disable-64bit-build CFLAGS=-m32 LDFLAGS=-m32 CXXFLAGS=-m32


Arch Linux
----------

An example PKGBUILD and needed scripts are provided in misc/archlinux.
Review and edit the PKGBUILD to suit your preferred configuration and
build situation (eg. building from a tarball vs building from SVN).
Run `makepkg` in that directory to build the package.


Ubuntu/Debian
-------------

Users of Ubuntu, Debian, and other similar packaging distributions of
Linux will need to ensure that a few essentials are in place before
you will be able to compile BRL-CAD.

Following the build instructions in the INSTALL file, if you are
starting from configure then you will need:

gcc (3+, e.g. 4.0.3)
make (e.g. gnu make 3.8.0)

If you are starting from autogen.sh you will need a few additional
tools that comprise the GNU Autotool toolset:

autoconf (2.50+, e.g. 2.59)
automake (1.6+, e.g. 1.9)
libtool (1.4+, e.g. 1.5)

All three of those have implicit dependencies on things like m4, perl,
cpp, and more.

If you installed automake version 1.4, you will need to install 1.9
and update the alternatives entry for automake so that it refers to
version 1.9 and not version 1.4:

sudo update-alternatives automake /usr/bin/automake-1.9

You will also want to make sure that you have the X11 development
headers installed:

apt-get install xserver-xorg-dev libx11-dev libxi-dev

Required: make (>= 3.8.0), bison, flex, xserver-xorg-dev, libx11-dev, libxi-dev
Optional: libpng-dev, zlib1g-dev, tcl8.5-dev (>= 8.5), tk8.5-dev (>= 8.5), tcl3-dev, itk3-dev, iwidgets4, blt-dev


Redhat/Fedora
-------------

To determine what particular version of Redhat or Fedora you are using, check
these files:

cat /etc/redhat-release
cat /etc/fedora-release


PPC64 Linux
-----------

If you happen to be installing on a ppc64 Linux system, the binaries
may not resolve correctly without being installed first.  Be sure to
install before testing applications (i.e., even before running the
benchmark or "make test").
