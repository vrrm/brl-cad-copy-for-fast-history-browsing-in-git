<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2009 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->


<refentry id='bwscale1'>

<refmeta>
  <refentrytitle>BWSCALE</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>bwscale</refname>
 <refpurpose>
   change the size of a black and white bw file
 </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
  <command>bwscale</command>    
    <arg choice='opt'>-r </arg>
    <arg choice='opt'>-s <replaceable>squareinsize</replaceable></arg>
    <arg choice='opt'>-w <replaceable>in_width</replaceable></arg>
    <arg choice='opt'>-n <replaceable>in_height</replaceable></arg>
    <arg choice='opt'>-S <replaceable>squareoutsize</replaceable></arg>
    <arg choice='opt'>-W <replaceable>out_width</replaceable></arg>
    <arg choice='opt'>-N <replaceable>out_height</replaceable></arg>
    <arg choice='opt'>-h </arg>
    <arg choice='opt'><replaceable>infile.bw</replaceable></arg>
    <arg choice='plain'><replaceable>outfile.bw</replaceable></arg>
</cmdsynopsis>
</refsynopsisdiv>


<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>
    <command>bwscale</command>
    <emphasis remap='I'>Bwscale</emphasis>
    will take a black and white
    <citerefentry><refentrytitle>bw</refentrytitle><manvolnum>5</manvolnum></citerefentry>
    format file of given dimensions and produce a larger or smaller
    scaled version.
    The
    <option>-w</option>
    and
    <option>-n</option>
    flags specify the input file width and number of scan lines in pixels.
    They can both be set to the same value via
    <option>-s.</option>
    Similarly, the
    <option>-W</option>
    and
    <option>-N</option>
    flags specify the desired output file width and number of scan lines in
    pixels.  They can both be set to the same value via
    <option>-S.</option>
  Defaults of 512 are assumed for any unspecified dimensions.</para>
  
  <para>
    By default,
    the algorithm used is bilinear interpolation if scaling up, and
    a box filter of arbitrary size if scaling down.  For the box filter a
    "square pixel" assumption is made; that is, all whole and fractional
    input pixels falling into a rectangle the size of an output pixel contribute
    equally according to the fraction of the output pixel area they cover.
    When interpolating, the edge pixels are preserved, i.e., all of the
    interpolated pixels fall inside of the edges.  This can be a bit surprising
    when doubling the size of a file, for example, as only the edge pixels
    will fall at exactly the location of an output pixel.
  </para>
  
  <para>Specifying the
  <option>-r</option>
  flag changes from bilinear interpolation to
  nearest neighbor interpolation, i.e., it enlarges the image by
  pixel replication.</para>
  
  <para>The
  <option>-h</option>
  flag causes the frame buffer to be
  used in high-resolution mode (1024 x 1024).
  This is important for frame buffers such as the Adage which operate
  differently depending on the display size.  Without this flag,
  the default size for the selected device will be used (usually
  the entire screen).</para>
</refsect1>

<refsect1 id='see_also'><title>SEE ALSO</title>
<para>
  <citerefentry><refentrytitle>brlcad</refentrytitle><manvolnum>1</manvolnum></citerefentry>, 
  <citerefentry><refentrytitle>bwcrop</refentrytitle><manvolnum>1</manvolnum></citerefentry>, 
  <citerefentry><refentrytitle>bwrect</refentrytitle><manvolnum>1</manvolnum></citerefentry>, 
  <citerefentry><refentrytitle>bw</refentrytitle><manvolnum>5</manvolnum></citerefentry>, 
  <citerefentry><refentrytitle>pixscale</refentrytitle><manvolnum>1</manvolnum></citerefentry>
</para>
</refsect1>

<refsect1 id='bugs'><title>BUGS</title>
<para>This program cannot scale up in one dimension and down in the other
at the same time.</para>
<para>
The box filter used for scaling down results in the usual high-frequency
ripple.</para>
<para>
Optional cubic interpolation would be nice.</para>
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
  </refsect1>

<refsect1 id='bug_reports'><title>BUG REPORTS</title>
<para>Reports of bugs or problems should be submitted via electronic
mail to devs@brlcad.org.</para>
</refsect1>
</refentry>

