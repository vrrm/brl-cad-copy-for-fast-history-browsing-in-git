TODO List for integrating MGED features into Archer

Intended as a "list of work to be done" for porting MGED features to Archer.  The absence of a feature
does not necessarily mean it is intended that this feature be added, particularly for "minor" features
or features that are currently not working well in MGED to start with - these are primarly discussion
points.

Starting with a "walkthrough" of the MGED menu:

     MGED Feature                   |         Archer Status of Feature or Alternative to Feature
------------------------------------------------------------------------------------------------------
Create New Database                 |                             DONE
Open Existing Database              |                             DONE (command line needs opendb)
Import Database	                    |    dbconcat available on command line, no menu options as in MGED
Export Database                     |    keep available on command line, no GUI menu options
Load Script                         |    no GUI dialog available, presumably command line works?
Raytrace Control Panel              |    older version in Archer, needs updating to current MGED
Render View options                 |    Will become Export View - INCOMPLETE
Preferences->Units                  |    Add to Preferences panel under the General tab
Preferences->Command Line Edit      |    Add to Preferences panel under the General tab
Preferences->Special Characters     |    Refers to globbing - per Bob this is currently being handled per
                                    |    command in Archer, so a global setting may not make sense.  A
                                    |    possible alternative is to have two lists of commands - globbing
                                    |    and non-globbing - and the ability to select and move them from
                                    |    one list to the other (kind of like traditional ftp clients) in
                                    |    a Preferences tab
Preferences->Colors                 |    INCOMPLETE - need to incorprate relevant settings in appropriate
                                    |    configuration panels, some of which aren't implemented in Archer
Preferences->Fonts                  |                    INCOMPLETE, lower priority
Create/Update .mgedrc               |    .archerrc exists, needs to be expanded as more preferences are
                                    |    implemented in Archer - ONGOING
Clear Command Window                |    clear command works on command line, Menu item judged not 
                                    |    necessary - DONE
Exit                                |                             DONE
Primitive Selection                 |    Need to integrate this functionality into the "component pick"
                                    |    button - straightforward enough, button already reports what is
                                    |    "picked" and just needs to do the internal logic to focus the
                                    |    pick for editing operations
Matrix Selection                    |    Some work needed to do this correctly - implement a matrix
                                    |    tab that is shown for every object in the right panel, which will
                                    |    have controls to set things like translation vectors and rotations
                                    |    to generate the matricies that hang "above" objects.  Do NOT, at
                                    |    the GUI level, allow direct numerical editing of matrix values - that
                                    |    is an invitation to invalid matricies and not especially intuitive.
                                    |    Ensure working command line level functionality for direct matrix
                                    |    manipulation if needed.  For items without a matrix show the tab with
                                    |    either no values specified or the identity matrix as the starting
                                    |    template.
Primitive Editor                    |    Functionality covered elsewhere, can be dangerous in that it accepts
                                    |    invalid inputs - remove
Combination Editor                  |    Functionality covered in General tab for objects in right panel, 
                                    |    except need color picker for rgb attribute - remove as separate entity
Attribute Editor                    |    This is a more general case of what is currently the General tab in
                                    |    Archer for objects - make this into a CAD widget with the layout of
                                    |    the General tab and the functionality of the Attribute editor -
                                    |    General tab will become Attribute tab.  Will need to be able to 
                                    |    customize the standard attribute/value display to support things like
                                    |    the color picker for rgb attribute
Geometry Browser                    |    Functionality should be covered by tree viewer - to ensure proper
                                    |    ability to easily select objects, need tabbed tree view widget with
                                    |    multiple trees - the current global tree in one tab, a flat list of
                                    |    all objects in another (e.g. like the output of ls - select any item
                                    |    as a tree root node), in another display tree(s) currently visible in
                                    |    display (simiilar to what who outputs, just with the ability to expand
                                    |    trees).  Probably some other possible modes to explore, even make
                                    |    configurable.
Create Primitives                   |    This functionality will be handled by the graphical primitive buttons
                                    |    make them into drop-down lists categorized like the MGED Create
                                    |    Primitive menu, hopefully with example images for all types.
View                                |                             DONE  (duplicated elsewhere)
ViewRing			    |    Useful aspects handled by loadview and saveview - may be worth reworking
                                    |    later but not a high priority currently.
Settings -> Mouse Behavior          |    Per item breakdown:
         Pick Edit Primitive        |    Replaced by combination of "component pick" button and tree widget
         Pick Edit Matrix           |    Replaced by matrix tab discussed under "Matrix Selection" item
         Pick Edit Combination      |    Replaced by combination of "component pick" button and tree widget
         Sweep Raytrace-Rect.       |    INCOMPLETE (will be handled differently in the UI as well, not under Settings)
         Pick Raytrace Objects      |    UNKNOWN
         Query Ray                  |    INCOMPLETE (nirt functionality exists, need mouse mode)
         Sweep Paint Rectangle      |    UNKNOWN
         Sweep Zoom Rectangle       |    UNKNOWN
Settings -> Transform               |    Angle/Distance Cursor not implemented yet
Settings -> Constraint Coords.      |                            UNKNOWN
Settings -> Rotate About            |                           INCOMPLETE?
Settings -> Active Pane             |                             DONE
Settings -> Apply To                |                            UNKNOWN
Settings -> Query Ray Effects       |                           INCOMPLETE
Settings -> Grid                    |                           INCOMPLETE
Settings -> Grid Spacing            |                           INCOMPLETE
Settings -> Framebuffer             |                           INCOMPLETE
Settings -> View Axes Position      |                             DONE (handled in preferences panel)
Modes -> Draw Grid                  |                           INCOMPLETE
Modes -> Snap to Grid               |                           INCOMPLETE
Modes -> FrameBuffer Active         |    Available, not as conveniently as in MGED
Modes -> Listen for Clients         |                            UNKNOWN
Modes -> Persistend Sweep Rect.     |                            UNKNOWN
Modes -> Angle/Dist. Cursor         |   INCOMPLETE (duplicated in MGED Settings->Transform?)
Modes -> Faceplate                  |                            UNKNOWN
Modes -> Axes                       |   View and Model DONE, does Edit apply in Archer?
Modes -> Display Manager            |   Archer is ogl only at the moment
Modes -> Multipane                  |                             DONE
Modes -> Edit Info                  |                            UNKNOWN
Modes -> Status Bar                 |             DONE (in geometry display area, bottom)
Modes -> Collaborate                |                            UNKNOWN
Modes -> Rate Knobs                 |                            UNKNOWN
Modes -> Display Lists              |                            UNKNOWN
Misc -> Z Clipping                  |                            UNKNOWN
Misc -> Perspective                 |                            UNKNOWN
Misc -> Faceplate                   |                            UNKNOWN
Misc -> Faceplate GUI               |                            UNKNOWN
Misc -> Keystroke Forwarding        |                            UNKNOWN
Tools -> ADC Control Panel          |                          INCOMPLETE
Tools -> AniMate                    |                          INCOMPLETE                           
Tools -> Grid Control Panel         |                          INCOMPLETE
Tools -> Query Ray                  |                          INCOMPLETE
Tools -> Raytrace Control Panel     |                          INCOMPLETE
Tools -> Build Pattern Tool         |                          INCOMPLETE
Tools -> Color Selector             |                          INCOMPLETE
Tools -> Geometry Browser           |                          INCOMPLETE
Tools -> Overlap Tool               |                          INCOMPLETE
Tools -> Upgrade Database...        |                          INCOMPLETE
Tools -> Command Window             |                            UNKNOWN 
Tools -> Graphics Window            |                            UNKNOWN
Help -> Dedication                  |                          INCOMPLETE
Help -> About MGED                  |   Dialog available, contents need to be merged in
Help -> Command Manual Pages        |                          INCOMPLETE
Help -> Shift Grips                 |   Not needed in Archer?
Help -> Apropos                     |   INCOMPLETE, not working well in MGED either?
Help -> Manual                      |   INCOMPLETE, even in MGED needs web browser, should use docbook/tkhtml3




Other Issues
------------

* documentation (even for windows) should not be a .chm file in our
  repository.  the documentation should be generated from docbook
  source formatting.

* archer html documentation refers to an 'ae' command, yet only 'aet'
  exists.  either the docs need updating or ae added.

* archer help menu incorrectly calls archer commands "BRL-CAD Commands"

* opendb command doesn't seem to be available.

* archer reports "invalid command" on 'aet' and other commands until a
  database is opened.  it shouldn't.

* archer doesn't have the -c option to run command-line or sans Tk

* need to "port" archer to the C code startup that MGED uses

* add ability to undo transaction sets

* handle situation where undo history uses up available memory (delete oldest entries?)

* move undo transaction support to libged level

Tests to Pass
-------------

* The merged Archer/MGED app needs to be able to pass all the regression
  tests currently passed by MGED


New Features
------------

* undo ability for selection sequences, other actions currently assumed to not change 
  the db geometry (may be a natural consequence of transactional libged commands?)
