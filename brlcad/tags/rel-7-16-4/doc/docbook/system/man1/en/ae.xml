<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='ae'>
  
  <refmeta>
    <refentrytitle>AE</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>ae</refname>
    <refpurpose>Sets the view orientation for the <emphasis>mged</emphasis> display 
    by rotating the eye position about the center of the viewing cube.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>ae</command>    
      <arg choice='opt'>-i</arg>
      <arg choice='req'><replaceable>azimuth</replaceable></arg>
      <arg choice='req'><replaceable>elevation</replaceable></arg>
      <arg choice='opt'><replaceable>twist</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      Set the view orientation for the <emphasis>mged</emphasis> display by rotating 
      the eye position about the center of the viewing cube. The eye position is 
      determined by the supplied azimuth and elevation angles (degrees). The 
      <emphasis>azimuth</emphasis> angle is measured in the <emphasis>xy</emphasis> 
      plane with the positive <emphasis>x</emphasis> direction corresponding to an 
      azimuth of 0�. Positive azimuth angles are measured counter-clockwise about 
      the positive <emphasis>z</emphasis> axis. Elevation angles are
      measured from the <emphasis>xy</emphasis> plane with +90� corresponding to the 
      positive <emphasis>z</emphasis> direction and -90 corresponding to the negative 
      <emphasis>z</emphasis> direction. If an optional <emphasis>twist</emphasis> angle 
      is included, the view will be rotated about the viewing direction by the 
      specified <emphasis>twist</emphasis> angle. The <emphasis>-i</emphasis> option 
      results in the angles supplied being interpreted as increments.
    </para>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      The examples show how to change views by entering azimuth and elevation angles, 
      adding a twist angle, and having the angles supplied being treated as increments.
    </para>
    <example>
      <title>Top view</title>
      <para>
	<prompt>mged></prompt> <userinput>ae -90 90</userinput>
      </para>
    </example>
    
    <example>
      <title>Right-hand side view </title>
      <para>
	<prompt>mged></prompt><userinput>ae 270 0</userinput>
      </para>
      <para>
	The view is from the right-hand side.
      </para>
    </example>
    
    <example>
      <title>View from a specific azimuth and elevation with a twist angle.</title>
      <para>
	<prompt>mged></prompt><userinput>ae 35 25 10</userinput>
      </para>
      <para>
	The view is from azimuth 35, elevation 25, with a view rotated by 10 degrees.
      </para>
    </example>
    
    <example>
      <title>The azimuth and elevation angles supplied are treated as increments.</title>
      <para>
	<prompt>mged></prompt><userinput>ae -i 0 0 5</userinput>
      </para>
      <para>
	Rotates the current view through 5 degrees about the viewing direction.
      </para>
    </example>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>

