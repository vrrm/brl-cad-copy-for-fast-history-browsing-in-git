.\" -*- nroff -*-
.\" Copyright (c) 2006 Donal K. Fellows.
.\"
.\" See the file "license.terms" for information on usage and redistribution
.\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
.\"
.\" RCS: @(#) $Id$
.\"
.so man.macros
.TH mathop n 8.5 Tcl "Tcl Mathematical Operator Commands"
.BS
.\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
mathop \- Mathematical operators as Tcl commands
.SH SYNOPSIS
package require \fBTcl 8.5\fR
.sp
\fB::tcl::mathop::!\fR \fInumber\fR
.br
\fB::tcl::mathop::~\fR \fInumber\fR
.br
\fB::tcl::mathop::+\fR ?\fInumber\fR ...?
.br
\fB::tcl::mathop::\-\fR \fInumber\fR ?\fInumber\fR ...?
.br
\fB::tcl::mathop::*\fR ?\fInumber\fR ...?
.br
\fB::tcl::mathop::/\fR \fInumber\fR ?\fInumber\fR ...?
.br
\fB::tcl::mathop::%\fR \fInumber number\fR
.br
\fB::tcl::mathop::**\fR ?\fInumber\fR ...?
.br
\fB::tcl::mathop::&\fR ?\fInumber\fR ...?
.br
\fB::tcl::mathop::|\fR ?\fInumber\fR ...?
.br
\fB::tcl::mathop::^\fR ?\fInumber\fR ...?
.br
\fB::tcl::mathop::<<\fR \fInumber number\fR
.br
\fB::tcl::mathop::>>\fR \fInumber number\fR
.br
\fB::tcl::mathop::==\fR ?\fIarg\fR ...?
.br
\fB::tcl::mathop::!=\fR \fIarg arg\fR
.br
\fB::tcl::mathop::<\fR ?\fIarg\fR ...?
.br
\fB::tcl::mathop::<=\fR ?\fIarg\fR ...?
.br
\fB::tcl::mathop::=>\fR ?\fIarg\fR ...?
.br
\fB::tcl::mathop::>\fR ?\fIarg\fR ...?
.br
\fB::tcl::mathop::eq\fR ?\fIarg\fR ...?
.br
\fB::tcl::mathop::ne\fR \fIarg arg\fR
.br
\fB::tcl::mathop::in\fR \fIarg list\fR
.br
\fB::tcl::mathop::ni\fR \fIarg list\fR
.sp
.BE
.SH DESCRIPTION
.PP
The commands in the \fB::tcl::mathop\fR namespace implement the same set of
operations as supported by the \fBexpr\fR command. All are exported from the
namespace, but are not imported into any other namespace by default. Note that
renaming, reimplementing or deleting any of the commands in the namespace does
\fInot\fR alter the way that the \fBexpr\fR command behaves, and nor does
defining any new commands in the \fB::tcl::mathop\fR namespace.
.PP
The following operator commands are supported:
.DS
.ta 2c 4c 6c 8c
\fB~\fR	\fB!\fR	\fB+\fR	\fB\-\fR	\fB*\fR
\fB/\fR	\fB%\fR	\fB**\fR	\fB&\fR	\fB|\fR
\fB^\fR	\fB>>\fR	\fB<<\fR	\fB==\fR	\fBeq\fR
\fB!=\fR	\fBne\fR	\fB<\fR	\fB<=\fR	\fB>\fR
\fB>=\fR	\fBin\fR	\fBni\fR
.DE
The behaviors of the operator commands are as follows:
.TP
\fB~\fR \fInumber\fR
.
Returns the bit-wise negation of \fInumber\fR. \fINumber\fR may be an integer
of any size.
.TP
\fB!\fR \fInumber\fR
.
Returns the boolean negation of \fInumber\fR. \fINumber\fR may be any numeric
value or any other form of boolean value.
.TP
\fB+\fR ?\fInumber\fR ...?
.
Returns the sum of arbitrarily many arguments. Each \fInumber\fR may be any
numeric value. If no arguments are given, the result will be zero.
.TP
\fB\-\fR \fInumber\fR ?\fInumber\fR ...?
.
Returns the either the negation of the first argument (if only one argument is
given) or the result of subtracting arbitrarily many additional arguments from
the first argument. Each \fInumber\fR may be any numeric value. At least one
argument must be given.
.TP
\fB*\fR ?\fInumber\fR ...?
.
Returns the product of arbitrarily many arguments. Each \fInumber\fR may be
any numeric value. If no arguments are given, the result will be one.
.TP
\fB/\fR \fInumber\fR ?\fInumber\fR ...?
.
Returns the either the reciprocal of the first argument (if only one argument
is given) or the result of dividing the first argument by arbitrarily many
additional arguments. Each \fInumber\fR may be any numeric value. At least one
argument must be given.
.TP
\fB%\fR \fInumber number\fR
.
Returns the integral modulus of the first argument with respect to the second.
Each \fInumber\fR must have an integral value.
.TP
\fB**\fR ?\fInumber\fR ...?
.
Returns the result of raising each value to the power of the result of
recursively operating on the result of processing the following arguments, so
\fB** 2 3 4\fR is the same as \fB** 2 [** 3 4]\fR. Each \fInumber\fR may be
any numeric value, though the second number must not be fractional if the
first is negative. If no arguments are given, the result will be one, and if
only one argument is given, the result will be that argument.
.TP
\fB&\fR ?\fInumber\fR ...?
.
Returns the bit-wise AND of each of the arbitrarily many arguments. Each
\fInumber\fR must have an integral value. If no arguments are given, the
result will be minus one.
.TP
\fB|\fR ?\fInumber\fR ...?
.
Returns the bit-wise OR of each of the arbitrarily many arguments. Each
\fInumber\fR must have an integral value. If no arguments are given, the
result will be zero.
.TP
\fB^\fR ?\fInumber\fR ...?
.
Returns the bit-wise XOR of each of the arbitrarily many arguments. Each
\fInumber\fR must have an integral value. If no arguments are given, the
result will be zero.
.TP
\fB<<\fR \fInumber number\fR
.
Returns the result of shifting the first argument left by the number of bits
specified in the second argument. Each \fInumber\fR must have an integral
value.
.TP
\fB>>\fR \fInumber number\fR
.
Returns the result of shifting the first argument right by the number of bits
specified in the second argument. Each \fInumber\fR must have an integral
value.
.TP
\fB==\fR ?\fIarg\fR ...?
.
Returns whether each argument is equal to the arguments on each side of it in
the sense of the \fBexpr\fR == operator (\fIi.e.\fR, numeric comparison if
possible). If fewer than two arguments are given, this operation always
returns a true value.
.TP
\fBeq\fR ?\fIarg\fR ...?
.
Returns whether each argument is equal to the arguments on each side of it
using exact string comparison. If fewer than two arguments are given, this
operation always returns a true value.
.TP
\fB!=\fR \fIarg arg\fR
.
Returns whether the two arguments are not equal to each other, in the sense of
the \fBexpr\fR != operator (\fIi.e.\fR, numeric comparison if possible).
.TP
\fBne\fR \fIarg arg\fR
.
Returns whether the two arguments are not equal to each other using exact
string comparison.
.TP
\fB<\fR ?\fIarg\fR ...?
.
Returns whether the arbitrarily-many arguments are ordered, with each argument
after the first having to be strictly more than the one preceding it.
Comparisons are performed preferentially on the numeric values. If fewer than
two arguments are present, this operation always returns a true value.
.TP
\fB<=\fR ?\fIarg\fR ...?
.
Returns whether the arbitrarily-many arguments are ordered, with each argument
after the first having to be equal to or more than the one preceding it.
Comparisons are performed preferentially on the numeric values. If fewer than
two arguments are present, this operation always returns a true value.
.TP
\fB>\fR ?\fIarg\fR ...?
.
Returns whether the arbitrarily-many arguments are ordered, with each argument
after the first having to be strictly less than the one preceding it.
Comparisons are performed preferentially on the numeric values. If fewer than
two arguments are present, this operation always returns a true value.
.TP
\fB>=\fR ?\fIarg\fR ...?
.
Returns whether the arbitrarily-many arguments are ordered, with each argument
after the first having to be equal to or less than the one preceding it.
Comparisons are performed preferentially on the numeric values. If fewer than
two arguments are present, this operation always returns a true value.
.TP
\fBin\fR \fIarg list\fR
.
Returns whether the value \fIarg\fR is present in the list \fIlist\fR.
.TP
\fBni\fR \fIarg list\fR
.
Returns whether the value \fIarg\fR is not present in the list \fIlist\fR.
.SH EXAMPLES
The simplest way to use the operators is often by using \fBnamespace path\fR
to make the commands available. This has the advantage of not affecting the
set of commands defined by the current namespace.
.CS
namespace path {::tcl::mathop ::tcl::mathfunc}

\fI# Compute the sum of some numbers\fR
set sum [\fB+\fR 1 2 3]

\fI# Compute the average of a list\fR
set list {1 2 3 4 5 6}
set mean [\fB/\fR [\fB+\fR {expand}$list] [double [llength $list]]]

\fI# Test for list membership\fR
set gotIt [\fBin\fR 3 $list]

\fI# Test to see if a value is within some defined range\fR
set inRange [\fB<=\fR 1 $x 5]
.CE
.SH "SEE ALSO"
expr(n), mathfunc(n), namespace(n)
.SH KEYWORDS
command, expression, operator
