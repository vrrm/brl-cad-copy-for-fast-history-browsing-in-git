The Installation Guide to BRL-CAD
=================================

Please read this document if you are interested in installing BRL-CAD.

This document covers the basics of installing BRL-CAD from either a
source or binary distribution.  Please see the 'Reporting Problems'
section if you run into any trouble installing BRL-CAD.

Some platforms have additional platform-specific documentation
provided in the doc/ directory of the source distribution that should
be consulted if that is the platform you are installing on.  This
presently includes the following:

  doc/README.MacOSX	-- Apple Mac OS X
  doc/README.IRIX	-- SGI IRIX and IRIX64


TABLE OF CONTENTS
-----------------
  Introduction
  Table of Contents
  Quick Installation
  Installing from Source
  Installing from Binary
  Configuration Options
  Compilation Options
  Installation Options
  Post-Installation 
  Testing an Install
  Reporting Problems


QUICK INSTALLATION
------------------

For the impatient or simplistic, the following should compile, test,
and install BRL-CAD quickly into the /usr/brlcad directory.  If you
have a configure script, run the following:

  ./configure
  make
  make benchmark
  make install   # as root, e.g. sudo make install
  make test

If you don't have a configure script, run the following to generate
the script then proceed with the steps above:

  sh autogen.sh

If any of the listed steps fail, then something unexpected happened.
See the reporting problems section of this document to report the
problem.

Once installed, add /usr/brlcad/bin to your path, and you should be
able to run one of the 400+ applications that constitute BRL-CAD.  For
example, to run the MGED solid modeler:

PATH=/usr/brlcad/bin:$PATH ; export PATH
mged

If you use tcsh or another C-shell based command shell, use this
instead:

set path=( /usr/brlcad/bin $path ) ; rehash
mged


INSTALLING FROM SOURCE
----------------------

There are a couple means to obtain the BRL-CAD sources, via either a
CVS checkout/export or a source distribution tarball.  Using the
latest CVS sources is recommended where possible, since it will have
the latest changes.


Starting From a CVS Checkout/Export:

If you have obtained the BRL-CAD sources from the CVS revision control
system, you will need to prepare the build for configuration:

  sh autogen.sh

This step requires that you have the GNU Build System (GBS) installed,
which includes a sufficiently recent version of Autoconf, Automake,
and Libtool.  Running autogen.sh will verify that the versions of each
are sufficient and will generate the `configure' script.  If you do
not have sufficient versions of the GBS components installed, you will
either need to install/ugrade them, run autogen.sh on another system
and them copy the files over, or start with a source tarball
distribution (where autogen.sh is automatically run for you).

Once autogen.sh is sucessfully run somewhere, you can continue with
the steps shown next for starting from a source distribution.


Starting From a Source Distribution:

There are many different ways to build BRL-CAD and depending on what
you need/want will determine which configuration options you should
use.  See the Configuration Options section below for details on how
to go about selecting which options are appropriate for you.

By default, the default configuration will prepare the build system
for installation into the /usr/brlcad directory (the --prefix option
may be used to change that).  This tradition goes back a couple
decades and is a convenient means to isolate the BRL-CAD solid
modeling system from your system, resolves conflicts, facilitates
uninstalls, and simplifies upgrades.  The default configuration is
performed by running the `configure' script:

  ./configure

By default, an unoptimized debug build of BRL-CAD will be configured
for compilation.  To obtain an optimized build (for example, for
BRL-CAD Benchmark performance testing), use the --enable-optimized
configure option:

  ./configure --enable-optimized

By default, all components and functionality will be built except
jove.  However, BRL-CAD does require and include several 3rd party
components.  If your system does not include a sufficient version of
those required 3rd party components, they will be automatically
configured for compilation.  You can force any one of those components
on or off via --enable-FEATURE and --disable-FEATURE arguments to
configure:

  ./configure --enable-termlib --disable-png

See the Configuration Options below for more details on all of the
possible settings.

Once configured, you should be able to succesfully build BRL-CAD via
make:

  make

See the Compilation Options section in this document for more details
on compile-time options including options for parallel build support.


Testing a Compilation:

Once the compilation is complete, you can test it before and after
installation.  To test a compilation of BRL-CAD before installation,
you can run the BRL-CAD benchmark.  The benchmark will report if the
results are correct, testing a majority of the core functionality of
BRL-CAD in addition to testing your system's performance:

  make benchmark

See the Testing An Install section of this document for more details
on testing your BRL-CAD distribution.


Installing a Compilation:

After the build successfully completes and assuming the benchmark also
produces correct results, installation may begin.  Like any package,
you must have sufficient filesystem permissions to install.  To
install into a system location, you can generally either become a
super user via the su or sudo commands:

  sudo make install

See the Installation Options section of this document for more details
on BRL-CAD installation options and post-install environment
preparations.


INSTALLING FROM BINARY
----------------------

There are a variety of different kinds of BRL-CAD binary
distributions.  Some of the binary disributions are sufficiently
generic and are simply a binary commpressed tarball distribution.
Others are specific to a particular platform such as Debian, Mac OS X,
FreeBSD, etc.


Generic Binary Distributions:

For the unspecialized binary distributions that are basically
compressed tarballs of the installation root, they should contain the
entire hierarchy of the distribution.  That is to say that they
contain /usr/brlcad in it's entirety so that if you decompress, you
will have a `usr' directory that contains a single `brlcad' directory:

gunzip brlcad-7.2.4_linux_ia64.tar.gz
tar -xvf brlcad-7.2.4_linux_ia64.tar
sudo mv usr/brlcad /usr/.

Of course, there are other compression options possible including zip
and bzip2.  By default, BRL-CAD expects to be installed into
/usr/brlcad and MGED is not relocateable by default.  It's recommended
that you start from a source distribution if you would like to install
into an alternate installation location.

However, if you do desire to install and run BRL-CAD from a different
location, give it a try.. ;) The only problems encountered should be
with running the MGED solid modeler where you will need to set the
BRLCAD_ROOT environment variable to your different install location
(e.g. /usr/local).  If this doesn't work (some platforms are more
problematic than others), you will need to compile and install from a
source distribution.


Mac OS X Disk Mounting Image:

Mount the .dmg and run the Installer .pkg contained therein.  This
will install into /usr/brlcad and will only require confirming that
your environment is set up properly (i.e. add /usr/brlcad/bin to your
path) as described in this document's Installation Options section.


CONFIGURATION OPTIONS
---------------------

By default, BRL-CAD is configured to build the entire package and will
install completely isolated into the /usr/brlcad directory.
Configuration will prepare the build for an unoptimized compilation by
default and will attempt to utilize required system libraries if they
are available, otherwise compiling the required library dependencies
as needed.  Run `./configure --help' for a list of all of the possible
configuration options.

Variables can be set in the environment passed to `configure'.
However, the build may run configure again during the build, and the
customized values of these variables may be lost.  In order to avoid
this problem, you should set them in the `configure' command line,
using `VAR=value'.  For example:

     ./configure CC=/usr/ucb/bin/cc

will cause the specified executable program to be used as the C
compiler.


COMPILATION OPTIONS
-------------------

If you are on a multiprocessor machine, you can compile in parallel
depending on the version of `make' being used.  For the GNU and BSD
make, you can use the -j argument.  For example, to use 4 CPUs:

  make -j4

With the IRIX compiler, the -P option enables parallel build mode and
the PARALLEL environment variable sets the number of CPUs (default is
two):

  PARALLEL=4 make -P


INSTALLATION OPTIONS
--------------------

By default, `make install' will install BRL-CAD's files into
`/usr/brlcad/bin', `/usr/brlcad/man', etc.  You can specify an
installation prefix other than `/usr/brlcad' by giving `configure' the
option `--prefix=PATH'.


Setting Up the Path:

Once installed, you will need to add /usr/brlcad/bin to your system
PATH.  For Bourne shell users, you can run the following:

  PATH=/usr/brlcad/bin:$PATH ; export PATH

For C shell users, this should do the same thing:

  set path=( /usr/brlcad/bin $path ) ; rehash

If you would like to provide BRL-CAD to multiple users, your shell
environment scripts should be edited so that /usr/brlcad/bin is added
to the system path.  This usually entails editing /etc/profile,
/etc/csh.login, /etc/bashrc, and possibly other files depending on
your platform.


Setting Up the Manual Pages Path:

It may be useful to add /usr/brlcad/man to your MANPATH in the same
manner as shown above for PATH.  Conversely, you could use the
`brlman' command that is already preconfigured to locate the BRL-CAD
manpages.


TESTING AN INSTALL
------------------

After BRL-CAD is installed, you can test the installation via:

  make test

This will run a series of tests on the installed sources to make sure
that they behave as they should.  Similarly, simply running `rt'
and/or `mged' are good tests of basic functionality.  Running `rt'
without any options should display version information and a usage
message.  Running `mged' without any options will start the GUI-based
solid modeler applicaation.


REPORTING PROBLEMS
------------------

Please report any bugs encountered to the project bug tracker at
http://sourceforge.net/tracker/?group_id=105292&atid=640802

Similarly, please post any request for feature enhancements or support
to http://sourceforge.net/tracker/?group_id=105292&atid=640805 and
http://sourceforge.net/tracker/?group_id=105292&atid=640803
respectively.


---
$Revision$
