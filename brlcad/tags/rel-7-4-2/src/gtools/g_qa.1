.\" Set the interparagraph spacing to 1 (default is 0.4)
.PD 1v
.\"
.\" The man page begins...
.TH G_QA 1
.SH NAME
g_qa \- a BRL-CAD geometry analysis tool
.SH SYNOPSIS
.BR "g_qa " "[options] model.g objects"
.SH DESCRIPTION
The 
.I g_qa 
program computes and reports the weight and volume of the 
.I objects
specified from the given 
.I model.g
geometric description.
It also reports overlaps.
Only the 
.I objects
from the database specified on the command line are analyzed.
It works by shooting grids of rays from the three axis-aligned directions (sometimes called 
\fIviews\fR).  
The resulting volume/weight calculations for each view are compared to each other.
The grid of rays is progressively refined until the results from all three views
agree within a user-specifiable tolerance, or a limit on grid refinement is reached.
.P
For weight computation, the density of every region must be specified.  
Densities are specified as an index in a table of density values.
This index is stored in the 
.I GIFTmater 
attribute of each region.
.P
The density table consists of three columns: 
.IP
.B An integer index value.
This is the value the to which the \fIGIFTmater\fR attribute will be set to select this material for the region.
.IP
.B A floating point density value.
This is the density for the material, and is specified in \fBgrams/cc\fR. 
.IP
.B An optional name string
This is a name or description the material.
.in -5
.P
An example file might look like the following:
.IP
1	7.84		Carbon Steel
.br
2	7.82		Carbon Tool Steel
.br
3	2.7		Aluminum, 6061-T6
.br
4	2.74		Aluminum, 7079-T6
.br
5	8.9		Copper, pure
.br
6	19.32		Gold, pure
.br
7	8.03		Stainless, 18Cr-8Ni
.br
8	7.47		Stainless 27Cr
.br
9	7.715		Steel, tool
.P
The table is typically created in an external file using a text editor.  
Validation of the table is typically performed using the external file (see the 
.I \-f
option below).
The file is imported to the database as the binary object
.B _DENSITIES
when complete.  To import the text file into the database, the following comand is used:
.IP
mged>
.B dbbinary \-i u c _DENSITIES 
.I filename
.SH VOLUME WEIGHT REPORTING
If weight calculation is selected, for each 
.I object
specified on the command line, volume and weight is calculated and reported.
.SH OVERLAP REPORTING
For each pair of objects that overlap, the tool reports the two regions that overlap, the maximum line-of-sight thickness of the overlap, and the in-hit location of the ray that caused that maximum overlap thickness.
.SH OPTIONS
.TP
.B \-A \fR\fIanalysis_flags\fR
Specifies which computations are to be performed and reported.
The 
.I analysis_flags
parameter is one or more of the following:
.in +5
.sp
\fBvalue	report generated\fR
.br
A		all
.br
a		adjacent different air
.br
b		bounding box dimensions
.br
e		exposed air
.br
g		gaps/voids
.br
o		overlaps
.br
v		volume
.br
w		weight
.sp
.in -5
Only those reports requested will be provided.  The default is 
.B -A A
to produce all reports.

.B Adjacent different air:
Detects air volumes which are next to each other but have different air_code values applied to the region.  
This would typically indicate that the regions are different types of air, 
such as crew_air (which fills the crew compartment of a vehicle) and engine_air (which surrounds the engine).
When these different types of air adjoin each other, it is generally considered a modeling error.

.B Bounding box dimensions:
Reports the dimensions of an axis-aligned box which fully encloses the 
\fIobjects\fR.

.B Exposed air:
This causes checks to be made to see if the ray encounters air regions before (or after all) solid objects.
Typically, only the air inside a building or vehicle is modeled if the purpose of the model is to support analysis of
that single structure/vehicle.
There are exceptions, such as when modeling larger environments for more extended analysis purposes.

.B Gaps/voids:
This reports when there is more than
.I overlap_tol_dist
(see the
.B \-t
option below)
between objects on the ray path.  Note that not all gaps are errors.  
For example, gaps between a wheel and a fender are expected (unless external air is modeled).
Typically, users should perform gap analysis on contained subsets of a model (such as passenger compartments)
rather than on whole vehicles.

.B Overlaps:
are two regions which occupy the same space.  
In the real world, two objects may not occupy the same space.  
This check is sometimes also known as \fIinterference checking\fR.
Two objects must overlap by at least 
.I overlap_tol_dist
(see the
.B \-t
option below)
to be considered to overlap.  Overlap testing never causes the grid spacing to
be refined, so it is important to specify the desired spacing for the initial
grid.  See the 
.B \-g 
option below for details on setting the grid spacing.

.B Volume:
Computes the volume of the 
.I objects
specified on the command line.

.B Weight:
Computes the weight of the 
.I objects
specified on the command line.
.TP
.B \-a \fR\fIazimuth_deg\fR
.B Not Implemented.
.br
Sets a rotation (in degrees) of the coordinate system by a given amount about the Z axis.  
The default is 0. 
See also:
.B \-e
.TP
.B \-e \fR\fIelevation_deg\fR
.B Not Implemented.
.br
Sets a rotation (in degrees) of the coordinate system by a given elevation from the XY plane (rotation about X axis?).  
The default is 0.
See also
.B \-a
.TP
.B \-f \fR\fIfilename\fR
Specifies that density values should be taken from an external file instead of from the 
.B _DENSITIES 
object in the database.
This option can be useful when developing the density table with a text editor, prior to importing it to the geometric database.
.TP
.B \-g \fR\fIgrid_spacing[,lim]\fR
Specifies the initial spacing between rays in the grids,
and optionally a limit on how far the grid can be refined.
The first value indicates the inital spacing between grid rays.  
If the optional second argument, "\fI,lim\fR",
is specified, it indicates a lower bound on how fine the gridspacing may get before computation is terminated.
The grid spacing may be specified with units.  For example:
.B 5 mm
or 
.B 10 in.
If units are not provided, millimeters are presumed to be the units.
.IP
The default values are 50.0 mm and 0.5 mm, which is equivalent to specifying: 
.B \-g 50.0mm,0.5mm
.TP
.B \-G
.br
.B Not Implemented.
.br
Specifies that the program should create new 
.I assembly combinations
in the geometry database to represent the overlap pairs.  
This flag is meaningless if overlap reporting is not turned on with the 
.B \-A
option.
If regions 
.I rod.r
and 
.I disk.r
overlap, this option will cause the creation of an assembly called _OVERLAP_rod.r_disk.r which includes the following items:
.in +5
.br
.I rod.r
.br
.I disk.r
.br
.I _OVERLAP_lines_rod.r_disk.r
.in -5

The last item is an object to represent the overlapping area so that it can be easily seen.  
The default is that no groups are created.
.TP
.B \-n \fR\fInum_hits\fR
Specifies that the grid be refined until each region has at least 
.I num_hits
ray intersections.
This limit is not applied per-view, but rather per-analysis.
So for example, it is accepted that a thin object might not be hit from one view at all, but hit when shooting from other views.
.IP
The default is 1.  This indicates that each region must be intersected by a ray at least once during the analysis.
If the grid spacing limit is reached, processing will end even if this criteria has not been met.
.TP
.B \-P \fR\fIncpu\fR
Specifies that
.I ncpu
CPUs should be used for performing the calculation.  
By default, all local CPUs are utilized.  
This option exists primarily to reduce the number of
computation threads from the machine maximum.
Note that specifying more CPUs than are present on the machine does not increase
the number of computation threads.  
.TP
.B \-r
Indicates that 
.I g_qa
should print per-region statistics (weight, volume) as well as the values for the objects specified on the command line.
.TP
.B \-S \fR\fIsamples_per_model_axis\fR
Specifies that the grid spacing will be initially refined so that at least
.I samples_per_axis_min
will be shot along each axis of the bounding box of the model.
For example, if the objects specified have a bounding box of 0 0 0 -> 4 3 2 and the grid spacing is 1.0, specifying the option
.B \-S 4
will cause the initial grid spacing to be adjusted to 0.5 so that 4 samples will be shot across the Z dimension of the bounding box.
The default is to assure 1 rays per model grid axis.

.TP
.B \-U \fR\fIuse_air\fR
Specifies the Boolean value 
.I use_air
which indicates whether regions which are marked as "air" should be retained and included in the raytrace.
.B Unlike other BRL-CAD raytracing applications the default is to retain air in the raytracing.
The 
.B \-U 0
option causes air regions to be discarded prior to raytracing.
Note that you probably don't want to turn off use_air when asking for various air checking analysis such as 
.B Air First/Last
or 
.B Contiguous unlike air
.TP
.B \-u \fR\fIdist,vol,wgt\fR
Specify the units used when reporting values.  Values are provided in the order 
\fIdistance\fR, \fIvolume\fR, and \fIweight\fR separated by commas.  For example:
.B \-u ``cm,cu ft,kg''
or
.B \-u ,,kg
Note that unit values with spaces in their names such as
.I cu ft
must be contained in quotes for the shell to keep the values together.

The default units are millimeters, cubic millimeters, and grams.
.TP
.B \-v
Turns on verbose reporting of computation progress.  This is useful for
learning how the computation is progressing, and what tolerances are causing
further computation to be necessary.
.TP
.B \-V \fR\fIvolume_tolerance[units]\fR
Specifies a volumetric tolerance value that the three view computations must be within for computation to complete.  
If volume calculation is selected and this option is not set, then the tolerance is set to 
1/10,000 of the volume of the model.
For large, complex objets (such as entire vehicles), this value might need to be 
set larger to achieve reasonable runtimes (or even completion).  
Given the approximate sampling nature of the algorithm, the
three separate view computations will not usually produce identical results.
.TP
.B \-W \fR\fIweight_tolerance[units]\fR
This is like the volume tolerance, \fB\-T\fR, but is applied to the weight computation results,
not the volume computation results.  

The weight computation tolerance is probably more appropriate when doing whole-vehicle analysis.
If weight computation is selected, it is set to a value equal to the weight of an object 1/100 
the size of the model, which is made of the most dense material in the table.

.SH EXAMPLES
.P
The following command computes the weight of an object called 
.I wheel.r
from the geometry database 
.I model.g
and reports the weight and volume, and checks for overlaps.
.IP
g_qa model.g wheel.r
.P
To check objects hull, turret, and suspension for overlaps only,
and to report overlaps and occurence of exposed air.
.IP
g_qa -A oe model.g hull turret suspension 
.P
