<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='ps'>

<refmeta>
  <refentrytitle>PS</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>ps</refname>
  <refpurpose>Temporarily attaches the <emphasis>Postscript</emphasis> display manager and outputs
	the current MGED display to the specified <emphasis>output_file</emphasis> in <emphasis>PostScript	</emphasis> format.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>ps</command>    
    
    <arg>-f <replaceable>font</replaceable></arg>
    <arg>-t <replaceable>title</replaceable></arg>
    <arg>-c <replaceable>creator</replaceable></arg>
    <arg>-s <replaceable>size_in_inches</replaceable></arg>
    <arg>-l <replaceable>line_width</replaceable></arg>
    <arg choice='req'><replaceable>output_file</replaceable></arg>

  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Temporarily attaches the <emphasis>Postscript</emphasis> display manager and outputs
	the current MGED display to the specified <emphasis>output_file</emphasis> in <emphasis>PostScript	</emphasis> format. The <emphasis>-f</emphasis> option allows the <emphasis>font</emphasis> to be user-	specified. The <emphasis>-t</emphasis> option allows the user to provide a title (the default is �No 	Title�). The <emphasis>-c</emphasis> option allows the user to specify the creator of the file (the 	default is �LIBDM dm-ps�). The <emphasis>�s</emphasis> specifies the size of the drawing in inches. The 	<emphasis>�l</emphasis> specifies the width of the lines drawn.
    </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The first example shows the use of the <command>ps</command> command to place a <emphasis>PostScript	</emphasis> version of the current MGED display in a specified file and give it a particular title.  The 	second example is much like the first except that a line width is specified and the title and output file 	names are different.
  </para>
       
  <example>
    <title>Place a <emphasis>PostScript</emphasis> version of the current MGED display in a specified file.</title>
    <para>
      <prompt>mged></prompt><userinput>ps -t "Test Title" test.ps</userinput>
    </para>
    <para>Places a <emphasis>PostScript</emphasis> version of the current MGED display in a file named 	<emphasis>test.ps</emphasis> and titles it �Test Title.�
    </para>
  </example>
  <example>
    <title>Specify the line width of a <emphasis>PostScript</emphasis> version of the current MGED display in a 	specified file.</title>
    <para>
      <prompt>mged></prompt><userinput>ps -l 10 -t "Test Fat Lines" fat_lines.ps</userinput>
    </para>
    <para>Places a <emphasis>PostScript</emphasis> version of the current MGED display with fat lines in a file 	named <emphasis>fat_lines.ps</emphasis> and titles it �Test Fat Lines.�
    </para>
  </example>
 

</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

