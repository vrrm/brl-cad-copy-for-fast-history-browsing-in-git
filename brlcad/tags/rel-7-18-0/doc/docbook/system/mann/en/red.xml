<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='red'>

<refmeta>
  <refentrytitle>RED</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>red</refname>
  <refpurpose>Creates a file describing the specified combination and starts an
	editor for the user to modify the combination.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>red</command>    
        <arg choice='req'><replaceable>combination</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Creates a file describing the specified combination and starts an
	editor for the user to modify the combination. The environment <emphasis>EDITOR</emphasis> 	variable will be used to select the editor. If <emphasis>EDITOR</emphasis> is not set, then 	<emphasis>/bin/ed</emphasis> will be used. All the attributes of BRL-CAD regions and 	<emphasis>combinations</emphasis> may be edited in this way. The region-specific attributes 	will be ignored if the <emphasis>combination</emphasis> is not a region and is not
	set to be a <emphasis>region</emphasis> during editing. It is not necessary to be in an 	editing mode to run this command. The <command>rm, r, comb, c,</command> and <command>g	</command> commands provide some basic combination editing capabilities.
  </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The example shows the use of the <command>red</command> command to edit a specified combination with the user's editor.
    </para>
     
  <example>
    <title>Create a file describing a specified combination and edit the combination.</title>
    <para>
      <prompt>mged></prompt><userinput>red group2</userinput>
    </para>
    <para>Edits the combination <emphasis>group2</emphasis> with the user�s editor of choice.
       
    </para>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

