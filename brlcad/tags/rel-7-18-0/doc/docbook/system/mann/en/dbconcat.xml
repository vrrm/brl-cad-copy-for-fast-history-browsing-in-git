<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='dbconcat'>
  
  <refmeta>
    <refentrytitle>DBCONCAT</refentrytitle>
    <manvolnum>nged</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>dbconcat</refname>
    <refpurpose>
      Concatenates an existing BRL-CAD database to the
      database currently being edited.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>dbconcat</command>    
      <arg choice='req'><replaceable>database_file</replaceable></arg>
      <arg choice='opt'><replaceable>prefix</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      Concatenates an existing <emphasis> BRL-CAD </emphasis> database to the
      database currently being edited. If a <emphasis> prefix</emphasis> is 
      supplied, then all objects from the <emphasis>database_file</emphasis> 
      will have <emphasis> prefix </emphasis> added to the beginning of their
      names. Note that each <emphasis> BRL-CAD</emphasis> object must have a unique 
      name, so care must be taken not to "dbconcat" a  database that has objects 
      with names the same as objects in the current database. 	The <command>dup</command> 
      command may be used to check for duplicate names. If the <command>dup</command> 
      command finds duplicate names, use the <emphasis>prefix</emphasis> option to both the 
      <command>dup</command> 	and <command>dbconcat</command> commands to find a 
      <emphasis>prefix</emphasis> that produces no duplicates.	If duplicate names 
      are encountered during the "dbconcat" process, computer-generated prefixes
      will be added to the object names coming from the <emphasis>database_file</emphasis> 
      (but member names appearing in combinations will not be modified, so this is a 
      dangerous practice and should be avoided).
    </para>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      The example shows the use of the <command>dbconcat</command> command to copy all objects in a particular
      database to the current database. It also shows how to add a particular prefix to every object copied.
    </para>
    <example>
      <title>Copy objects in a specified database to the current one and add a prefix to all the copied objects.</title>
      <para><prompt>mged></prompt> <userinput>dbconcat model_two.g two_</userinput></para>
      <para>
	Copies all the objects in <emphasis>model_two.g</emphasis> to the current database,
	but prefixes the name of every object copied with the string <emphasis>two_</emphasis>.
      </para>
    </example>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>

