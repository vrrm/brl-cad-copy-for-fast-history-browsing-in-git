<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='l'>

<refmeta>
  <refentrytitle>L</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>l</refname>
  <refpurpose>Displays a verbose description about the specified list of objects.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>l</command>    
    
    <arg>-r</arg>
    <arg choice='req'><replaceable>objects></replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Displays a verbose description about the specified list of objects.
	If a specified <emphasis>object</emphasis> is a path, then any transformation matrices 	along that <emphasis>path</emphasis> are applied. If the final <emphasis>path </emphasis>	component is a combination, the command will list the Boolean formula for the <emphasis>	combination</emphasis> and will indicate any accumulated transformations (including any in 	that <emphasis>combination</emphasis>). If a shader and/or color has been assigned to the 	<emphasis>combination</emphasis>, the details will be listed. For a region, its ident, air 	code, material code, and LOS will also be listed. For primitive shapes, detailed 	<emphasis>shape</emphasis> parameters will be displayed with the accumulated 	transformation applied. If the <emphasis>-r</emphasis>
	(recursive) option is used, then each <emphasis>object</emphasis> on the command line will 	be treated as a <emphasis>path</emphasis>. If the <emphasis>path</emphasis> does not end 	at a primitive <emphasis>shape</emphasis>, then all possible<emphasis>paths</emphasis> 	from that point down to individual <emphasis>shapes</emphasis> will be considered. The 	<emphasis>shape</emphasis> at the end of each possible <emphasis>path</emphasis> will be 	listed with its parameters adjusted by the accumulated transformation.
   </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The three examples show the use of the <command>l</command> command to display details about a region, shape parameters, and all possible paths starting with a particular designation. 
    
  </para>
  <example>
    <title>Display details about a particular region.</title>
    <variablelist>
      <varlistentry>
	   <term><prompt>mged></prompt> <userinput>l region1</userinput></term>
	   <listitem>
	     <para>Displays details about region1.
	      	     </para>
	   </listitem>
      </varlistentry>
     </variablelist>
  </example>
       
  <example>
    <title>Display shape parameters for a particular shape.</title>
    <para>
      <prompt>mged></prompt><userinput>l group1/group2/region1/shape3</userinput>
    </para>
    <para>Displays shape parameters for <emphasis>shape3</emphasis> with matrices applied from 	the <emphasis>path</emphasis>.
     </para>
  </example>
  
 <example>
    <title>Display all paths that start with a particular designation and end in a primitive 	shape.</title>
    <para>
      <prompt>mged></prompt><userinput>l -r a/b</userinput>
    </para>
    <para>Displays all paths that start with a particular designation and end in a primitive 	shape.  Also, the shape parameters with the accumulated transformation applied will be 	displayed.
    </para>
  </example>

</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

