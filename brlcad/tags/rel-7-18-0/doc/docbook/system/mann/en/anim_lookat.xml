<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='anim_lookat1'>
  <refmeta>
    <refentrytitle>ANIM_LOOKAT</refentrytitle>
    <manvolnum>nged</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>anim_lookat</refname>
    <refpurpose>
      create a view animation script or table which looks from 
      an eye point to a specified point.
    </refpurpose>
  </refnamediv>

  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>anim_lookat</command>    
      <arg choice='opt'>-f <replaceable>#</replaceable></arg>
      <arg choice='opt'>-v </arg>
      <arg choice='plain'><replaceable>in.table</replaceable></arg>
      <arg choice='plain'><replaceable>out.script</replaceable></arg>
    </cmdsynopsis>
    <cmdsynopsis>
      <command>anim_lookat</command>    
    <arg choice='plain'>- </arg>
    <group choice='opt'>
      <arg choice='plain'><replaceable>y</replaceable></arg>
      <arg choice='plain'><replaceable>q</replaceable></arg>
    </group>
    <arg choice='opt'>-v </arg>
    <arg choice='plain'><replaceable>in.table</replaceable></arg>
    <arg choice='plain'><replaceable>out.table</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>


  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      This program takes as input a seven-column animation table
      consisting of time, an eye point, and a "look-at point," and prints a
      view animation script to control the virtual camera.
      The camera always remains "right-side up"; that is, it yaws and pitches but
      doesn't roll. In cases where the camera looks vertically up or down, the
      yaw from the previous frame is retained. This avoids sudden jumps as much
      as possible.
    </para>
    
  </refsect1>
  
  <refsect1 id='options'><title>OPTIONS</title>
  <variablelist remap='TP'>
    <varlistentry>
      <term><option>-f#</option></term>
      <listitem>
	<para>Specify an integer with which to begin
	numbering frames. The default is 0.</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-y</option></term>
      <listitem>
	<para>
	  The output will be an animation table rather
	  than a view animation script. The output table has 7 columns:
	  a time column, three columns representing position, and three 
	  columns representing yaw, pitch, and roll.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-q</option></term>
      <listitem>
	<para>
	  The output will be an animation table rather than a view animation script.
	  The output table has eight columns: a time column, three position columns, and four columns
	  representing the x,y,z, and w coordinates of an orientation quaternion.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-v</option></term>
      <listitem>
	<para>
	  Include the viewsize in the output script or table. The viewsize is
	  calculated in every frame as twice the distance from the eye point to
	  the lookat point. If the output is a table, then the viewsize is printed
	  in the second column.
	</para>
      </listitem>
    </varlistentry>
  </variablelist>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>Carl J. Nuzman</para>
  </refsect1>
  
  <refsect1 id='copyright'><title>COPYRIGHT</title>
  <para>
    This software is Copyright (c) 1993-2010 by the United States
    Government as represented by U.S. Army Research Laboratory.
  </para>
  </refsect1>
  
  <refsect1 id='bug_reports'><title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;.
  </para>
  </refsect1>
</refentry>

