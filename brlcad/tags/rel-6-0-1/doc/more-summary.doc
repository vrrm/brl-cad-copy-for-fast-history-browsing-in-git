                            BRL-CAD Update

BRL-CAD is a software package consisting of a solid modeling system, a
library of ray-tracing routines, two lighting models, many image
handling utilities and other useful graphics software.   
This software package is indispensible to the all classes
of item-level modeling, as the tools which it contains support the
the generation, modification, and interrogation of computer-aided
design files (i.e. target descriptions) fundamental to all vulnerability,
neutron transport, weight, signature, etc. calculations.
In October
1988, release 3.0 was made available and consists of several updates
and improvements.  The BRL-CAD package is in its third generation and
represents over 150,000 lines of C source code.  Designed to be highly
portable, this software runs on hardware from a variety of vendors
offering the UNIX operating system.  To date, this package has been
sent to over 400 sites world-wide.
It is the major CAD package supporting weapons analyses (predominantly
vulnerability and signature calculations) used by many entities of
the DoD including the Army, Air Force, various intelligence agencies,
and various DoD (including DARPA) contractors.


                       FASTGEN/MGED Integration

The Air Force uses the FASTGEN faceted geometric data format while
the Army uses 
the MGED hierarchical file format and interactive editor
(of the BRL-CAD system above).
In this task,
the FASTGEN faceted geometry  
was merged into 
the MGED system.  Due to the modular, object-oriented design of the
MGED system, it was a straight-forward task to merely add the faceted
geometry as a "new" primitive.  In addition, the appropriate modules
for the faceted geometry were added to the associated ray-trace
library (LIBRT),  allowing utilization in all the LIBRT application
code.  This task has provided a single hybrid geometric data base capable
of supporting all applications and will eliminate the current
duplication of efforts
since geometry generated previously by hand in FASTGEN or via
MGED (in BRL-CAD) can be merged into a single, mixed data base
and interrogated (via ray tracing) through a single standard set
of CAD tools to support both Army and Air Force vulnerability
codes.
