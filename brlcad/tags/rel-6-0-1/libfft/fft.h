/*
 *			F F T . H
 *
 *  Author -
 *	Phil Dykstra
 *  
 *  Source -
 *	The U. S. Army Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005-5068  USA
 *  
 *  Distribution Status -
 *	Public Domain, Distribution Unlimited.
 *
 *  $Header$
 */

extern void splitdit( double X[], int N, int M );
extern void ditsplit(
	double x[],
	int	n,	/* length */
	int	m);	/* n = 2^m */
