<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='cpi'>

  <refmeta>
    <refentrytitle>CPI</refentrytitle>
    <manvolnum>nged</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>cpi</refname>
    <refpurpose>
      Copies <emphasis>old_tgc</emphasis>(an existing TGC shape) to a new TGC shape
      (<emphasis>new_tgc</emphasis>), positions the new TGC such that its base vertex
      is coincident with the center of the top of <emphasis>old_tgc</emphasis>, and 
      puts <emphasis>mged</emphasis> into the primitive edit state with 
      <emphasis>new_tgc</emphasis> selected for editing.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>cpi</command>    
      <arg choice='req'><replaceable>old_tgc new_tgc</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      Copies <emphasis>old_tgc</emphasis>(an existing TGC shape) to a new TGC shape
      (<emphasis>new_tgc</emphasis>), positions the new TGC such that its base vertex is 
      coincident with the center of the top of <emphasis>old_tgc</emphasis>, and puts 
      <emphasis>mged</emphasis> into the primitive edit state with <emphasis>new_tgc</emphasis> 
      selected for editing.  This command was typically used in creating models of wiring or
      piping runs; however, a pipe primitive has since been added to <emphasis>BRL-CAD</emphasis> 
      to handle such requirements.
    </para>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      The example shows the use of the <command>cpi</command> command to copy an existing TGC 
      shape to a new TGC shape and position the new shape with its base vertex coincident with 
      the center of the top of the existing shape.
    </para>
    <example>
      <title>Copy an existing TGC shape to a new TGC shape.</title>
      <para><prompt>mged></prompt> <userinput>cpi tgc_a tgc_b</userinput></para>
      <para>
	Copies <emphasis>tgc_a</emphasis> to <emphasis>tgc_b</emphasis> and translates
	<emphasis>tgc_b</emphasis> to the end of <emphasis>tgc_a</emphasis>.
      </para>
    </example>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>

