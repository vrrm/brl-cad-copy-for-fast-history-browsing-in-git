<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='attr'>
<refmeta>
  <refentrytitle>ATTR</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD MGED Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>attr</refname>
  <refpurpose> Used to create, change, retrieve, or view attributes of database
objects.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>attr</command>    
    <arg choice='opt'>get</arg>
    <arg choice='opt'>set</arg>
    <arg choice='opt'>rm</arg>
    <arg choice='opt'>append</arg>
    <arg choice='opt'>show</arg> 
    <arg choice='req'><replaceable>object_name</replaceable></arg>
    <arg choice='opt'><replaceable>arguments</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>
    Used to create, change, retrieve, or view attributes of database
    objects. The arguments for "set" and "append" subcommands are attribute
    name/value pairs. The arguments for "get," "rm," and "show" subcommands are
    attribute names. The "set" subcommand sets the specified attributes for the object.
    The "append" subcommand appends the provided value to an existing attribute, or
    creates a new attribute if it does not already exist. The "get" subcommand retrieves
    and displays the specified attributes. The "rm" subcommand deletes the specified
    attributes. The "show" subcommand does a "get" and displays the results in a user
    readable format. Note that the attribute names may not contain embedded white
    space, and if attribute values contain embedded white space, they must be surrounded
    by {} or double quotes.
  </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>
    The examples demonstrate the use of the <command>attr</command> command and subcommands to 
    assign and list attributes of database objects.
  </para>
  <example>
    <title>Assigns an attribute to an object.</title>
    <variablelist>
      <varlistentry>
	<term><prompt>mged</prompt><userinput>attr set region_1 comment {This is a comment for region_1}</userinput></term>
	<listitem>
	  <para>
	    Assigns an attribute named "comment" to <emphasis>region_1</emphasis>. Its 
	    value is "This is a comment for region_1."
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
  </example>
  
  <example>
    <title>List all the attributes for an object</title>
    <variablelist>
      <varlistentry>
	<term><prompt>mged</prompt><userinput>attr show region_1 comment</userinput></term>
	<para>Lists all the attributes for <emphasis>region_1</emphasis>.
	</para>
      </varlistentry>
    </variablelist>
  </example>
  
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

