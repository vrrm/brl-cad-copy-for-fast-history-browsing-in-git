<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->


<refentry id='analyze'>

  <refmeta>
    <refentrytitle>ANALYZE</refentrytitle>
    <manvolnum>nged</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>analyze</refname>
    <refpurpose>
      Displays the rotation and fallback angles, surface area, and
      plane equation for each face of the ARB specified on the command line. The total
      surface area, volume, and the length of each edge are also displayed.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>analyze</command>    
      <arg choice='req'><replaceable>arb_name</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      The <command>analyze</command> command displays the rotation and fallback 
      angles, surface area, and plane equation for each face of the ARB specified 
      on the command line. The total surface area, volume, and the length of each 
      edge are also displayed. If executed while editing an ARB, the arb_name may 
      be omitted, and the ARB being edited will be analyzed.
    </para>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      The example shows how to display information about an ARB's rotation and 
      fallback angles, surface area, and plane equation for its face. 
      Edge information is also displayed.
    </para>
    <example>
      <title>Analyze a particular <emphasis>ARB</emphasis></title>
      <para>
	<prompt>mged></prompt> <userinput>analyze arb_name</userinput>
      </para>
      <para>
	Rotation and fallback angles, surface area, and plane equation for 
	each face of the ARB specified on the command line are displayed along 
	with edge information. 
      </para>
    </example>
  </refsect1>

  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>

