#ifdef WIN32
#include "common.h"

#include <stdio.h>
#include "machine.h"
#include "bu.h"
#endif
const char version[] = "\
@(#) BRL-CAD Release 7.4.0   The IGES to BRL-CAD converter\n\
    Fri Sep 9 12:00:00 EST 2005, Compilation 1\n\
    bob@mako:/home/bob/src/brlcad/src/iges\n";
