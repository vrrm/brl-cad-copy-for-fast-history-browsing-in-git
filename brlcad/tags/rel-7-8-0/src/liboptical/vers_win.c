#ifdef WIN32
#include "common.h"

#include <stdio.h>
#include "machine.h"
#include "raytrace.h"
#include "optical.h"
#endif
const char liboptical_version[] = "\
@(#) BRL-CAD Release 7.6.7   The BRL-CAD Optical Shader Library\n\
    Mon Sep 12 16:00:00 EDT 2005, Compilation 1\n\
    bob@mako:/home/bob/src/brlcad/src/liboptical\n";
