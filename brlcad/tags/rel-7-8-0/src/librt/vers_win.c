#ifdef WIN32
#include "common.h"

#include <stdio.h>
#include <math.h>
#include "machine.h"
#include "vmath.h"
#include "raytrace.h"
#endif
const char rt_version[] = "\
@(#) BRL-CAD Release 7.6.7   The BRL-CAD Ray-Tracing Library\n\
    Mon Sep 12 16:00:00 EDT 2005, Compilation 1\n\
    bob@mako:/home/bob/src/brlcad/src/librt\n";
