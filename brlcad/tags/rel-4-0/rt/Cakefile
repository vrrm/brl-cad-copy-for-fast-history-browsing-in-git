/*
 *			rt/Cakefile
 *
 *  $Header: /usr/Backup/upgrade/brlcad/brlcad/rt/Cakefile,v 10.1 1991-10-12 06:42:28 mike Exp $
 */
#define CONFIGDEFS	RT_CONFIG

#define SRCDIR	rt
#define PRODUCTS	rt rtrad rtpp rtray rtshot rtwalk rtcheck \
			rtg3 rtcell rtxray rthide rtfrac rtrange \
			rtregis rtscale
#define	SRCSUFF	.c
#define MANSECTION	1

#include "../Cakefile.defs"
#include "../Cakefile.prog"

/* light.h material.h mathtab.h rad.h rdebug.h */
#define FILES	\
	rtshot \
	main opt do worker \
	view viewpp viewray viewrad viewcheck viewg3 \
	regionfix \
	shade sh_text sh_cloud mathtab \
	material refract sh_stack \
	sh_plastic sh_cook wray sh_spm sh_light \
	sh_marble sh_stxt sh_points

/*  The "top-half" (user interface) for the RT family of programs */
#define RTUIF	main.o opt.o do.o worker.o mathtab.o

/* Explicit composition of each product */

#define	RT_OBJ	RTUIF view.o wray.o material.o refract.o \
		shade.o sh_text.o sh_cloud.o sh_stack.o sh_plastic.o \
		sh_cook.o sh_spm.o sh_light.o sh_marble.o \
		sh_stxt.o sh_points.o

rt:	RT_OBJ LIBRT
	sh ../newvers.sh version "Ray-Tracer"
	CC CFLAGS -c vers.c
	CC LDFLAGS RT_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rt


#define RTRAY_OBJ	RTUIF viewray.o regionfix.o wray.o

rtray:	RTRAY_OBJ LIBRT
	sh ../newvers.sh version "RT Ray"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTRAY_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rtray

#define RTPP_OBJ	RTUIF viewpp.o

rtpp:	RTPP_OBJ LIBRT
	sh ../newvers.sh version "RT pretty-picture"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTPP_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rtpp

#define RTXRAY_OBJ	RTUIF viewxray.o

rtxray:	RTXRAY_OBJ LIBRT
	sh ../newvers.sh version "RT X-Ray"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTXRAY_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rtxray

#define RTRAD_OBJ	RTUIF viewrad.o

rtrad:	RTRAD_OBJ LIBRT
	sh ../newvers.sh version "RT Rad"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTRAD_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rtrad

#define RTSCAT_OBJ	RTUIF viewscat.o

rtbscat: RTSCAT_OBJ LIBRT LIBRAD
	sh ../newvers.sh version "RT BackScatter"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTSCAT_OBJ vers.o LIB_PRE''LIBRAD LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rtbscat

#define RTCHECK_OBJ	RTUIF viewcheck.o

rtcheck: RTCHECK_OBJ LIBRT
	sh ../newvers.sh version "RT Check"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTCHECK_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rtcheck

rtshot:	rtshot.o LIBRT
	CC LDFLAGS rtshot.o LIB_PRE''LIBRT LIBRT_LIBES LIBMALLOC LIBES -o rtshot

rtwalk:	rtwalk.o LIBRT
	CC LDFLAGS rtwalk.o LIB_PRE''LIBRT LIBRT_LIBES LIBMALLOC LIBES -o rtwalk

#define RTG3_OBJ	RTUIF viewg3.o regionfix.o

rtg3:	RTG3_OBJ LIBRT
	sh ../newvers.sh version "RTG3"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTG3_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rtg3

#define RTCELL_OBJ	RTUIF viewcell.o

rtcell:	RTCELL_OBJ LIBRT
	sh ../newvers.sh version "RTCELL"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTCELL_OBJ vers.o LIB_PRE''LIBRT LIBRT_LIBES LIB_PRE''LIBFB LIBFB_LIBES LIBMALLOC LIBES -o rtcell

#define RTHIDE_OBJ	RTUIF viewhide.o regionfix.o

rthide:	RTHIDE_OBJ LIBRT
	sh ../newvers.sh version "RT Hidden-Line Plot"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTHIDE_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rthide

#define RTFRAC_OBJ	RTUIF viewfrac.o regionfix.o 

rtfrac:	RTFRAC_OBJ LIBRT
	sh ../newvers.sh version "RT Volume Fractions"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTFRAC_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rtfrac

#define RTRANGE_OBJ	RTUIF viewrange.o regionfix.o 

rtrange:	RTRANGE_OBJ LIBRT
	sh ../newvers.sh version "RT Range Plot"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTRANGE_OBJ vers.o LIB_PRE''LIBRT LIB_PRE''LIBFB LIBFB_LIBES LIBRT_LIBES LIBMALLOC LIBES -o rtrange

#define RTDUMMY_OBJ	RTUIF viewdummy.o regionfix.o 

rtdummy:	RTDUMMY_OBJ LIBRT
	sh ../newvers.sh version "RT Dummy"
	CC CFLAGS -c vers.c
	CC LDFLAGS RTDUMMY_OBJ vers.o LIB_PRE''LIBRT LIBRT_LIBES LIB_PRE''LIBFB LIBFB_LIBES LIBMALLOC LIBES -o rtdummy

rtexample:	LIBRT rtexample.o
	CC LDFLAGS rtexample.o LIB_PRE''LIBRT LIBRT_LIBES LIB_PRE''LIBFB LIBFB_LIBES LIBMALLOC LIBES -o rtexample

rtregis:	LIBRT rtregis.o read-rtlog.o
	CC LDFLAGS rtregis.o read-rtlog.o LIB_PRE''LIBRT LIBRT_LIBES LIBMALLOC LIBES -o rtregis

rtscale:	LIBRT LIBWDB rtscale.o read-rtlog.o
	CC LDFLAGS rtscale.o read-rtlog.o LIB_PRE''LIBRT LIBRT_LIBES LIB_PRE''LIBWDB LIB_PRE''LIBPLOT LIBMALLOC LIBES -o rtscale

#include "../Cakefile.rules"
