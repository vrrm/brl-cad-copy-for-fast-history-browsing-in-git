#
# DbPage.itk
#
# This file provides a page for specifying the database to be loaded.
#

package require Itcl
package require Itk
package require Iwidgets 3.0

namespace eval RtWizard {

    package provide DbPage 1.0

    #
    # dbpage - Provides a lowercased access method for the 
    #          DbPage class.
    #
    proc dbpage {pathName args} {
	uplevel DbPage $pathName $args
    }

    #
    # Add options
    #
    option add *DbPage.borderWidth 2 widgetDefault
    option add *DbPage.labelPos wn widgetDefault
    option add *DbPage.listHeight 150 widgetDefault
    option add *DbPage.hscrollMode dynamic widgetDefault
    option add *DbPage.vscrollMode dynamic widgetDefault
    option add *DbPage.textBackground seashell
    
    #
    # Define the usual options
    #
    ::itk::usual DbPage {
	keep -activebackground \
		-activerelief \
		-background \
		-borderwidth \
		-cursor \
		-elementborderwidth \
		-foreground \
		-highlightcolor \
		-highlightthickness \
		-insertbackground \
		-insertborderwidth \
		-insertofftime \
		-insertontime \
		-insertwidth \
		-jump \
		-labelfont \
		-selectbackground \
		-selectborderwidth \
		-textbackground \
		-troughcolor
    }

    #
    # DbPage -
    #
    # The DbPage megawidget is very simple. It is a fileselector and
    # some help text.
    #
    # The DbPage is a support page, not a "step."
    #
    itcl::class DbPage {
	inherit ::iwidgets::Labeledframe

	#
	# Configuration variables
	#
	
	# None!
		
	public {
	    #
	    # methods
	    #
	    constructor {args} {};

	    method onSelect {}
	    method onDeselect {}
	    method getStepInfo {}
	   
	    method getDbFile {}
	}
	
	private {
	    method onOK {}
	    method onCancel {}
	}    
    }

    #--------------------#
    #   Public Methods   #
    #--------------------#    
    #
    # constructor - builds the Database Page
    #
    ::itcl::body DbPage::constructor {args} {
	
	#
	# Set up the childsite
	#
	set cs [ $this childsite ]
	$cs configure -relief groove -bd 2

	#
	# Create the help panel
	#
	itk_component add helpPanel {
	    ::iwidgets::scrolledtext $cs.#auto \
		    -hscrollmode none \
		    -vscrollmode none \
		    -visibleitems 80x3 \
		    -wrap word \
		    -relief flat \
		    -textfont $::RtWizard::helpFont
	} {
	    usual
	    ignore -textbackground
	    ignore -textfont
	}

	#
	# Add the help text.
	#
	$itk_component(helpPanel) insert end \
		"Please select the BRL-CAD database that contains the "
	$itk_component(helpPanel) insert end \
		"objects that you wish to render."
	$itk_component(helpPanel) configure -state disabled

	#
	# Create the fileselector for the database name.
	#
	itk_component add fileSelect { 
	    ::iwidgets::fileselectionbox $cs.#auto \
		    -height 340

	} {
	    usual
	}
	
	#
	# Look only for BRL-CAD databases
	#
	$itk_component(fileSelect) configure -mask "*.g"
	

	#
	# Create a button box for the OK button
	#
	itk_component add bbox {
	    ::iwidgets::buttonbox $cs.#auto
	} {
	    usual
	}

	$itk_component(bbox) add OK \
		-text "OK" \
		-command "[::itcl::code $this onOK]"

	$itk_component(bbox) add cancel \
		-text "Cancel" \
		-command "[::itcl::code $this onCancel]"
	
	$itk_component(bbox) default cancel

	#
	# Pack the items
	#
	pack $itk_component(helpPanel) \
		-side top \
		-fill x \
		-expand 1 
	pack $itk_component(fileSelect) \
		-fill both \
		-expand yes \
		-padx 4 \
		-pady 4 
	pack $itk_component(bbox) \
		-side right \
		-expand 0 \
		-pady 2
	
	#
	# Configure the labeled widget
	#
	$this configure -labeltext "Select a Database"
	$this configure -labelpos nw
	
	#
	# Evaluate any user supplied args.
	#
	eval itk_initialize $args
    }

    #
    # onSelect
    #
    # Makes any necessary changes to the Wizard configuration
    # when this page is selected.
    #
    itcl::body DbPage::onSelect {} {
	#
	# Nothing yet!
	#
    }

    #
    # onDeselect
    #
    # Cleans up when the page is deselected
    #
    itcl::body DbPage::onDeselect {} {	
	#
	# Nothing yet!
	#
    }

    #
    # getStepInfo
    #
    # In principal, returns a name for the step, and a variable that
    # can be queried for state. The database page is not a 'step', so
    # it returns "No Step"
    #
    itcl::body DbPage::getStepInfo {} {
	set a "No Step"
	set b "banana"
	return [list $a $b]
    }

    #
    # getDbFile - returns the currently selected file.
    #
    ::itcl::body DbPage::getDbFile { } {
	return [ $itk_component(fileSelect) get ]
    }

    #--------------------#
    #  Private Methods   #
    #--------------------#    
    #
    # onOK - called when the OK button is called
    #
    itcl::body DbPage::onOK {} {
	set ::RtWizard::dbFile [getDbFile]
	$::wizardInstance select exp
    }

    #
    # onCancel - called when the OK button is called
    #
    itcl::body DbPage::onCancel {} {
	exit
#	$::wizardInstance selectPrev
    }




}
#end namespace























