/*
 *			libtk/Cakefile
 */
#define SRCDIR	[[echo libtk''TCLTK_VERS]]
#define	SRCSUFF	.c
#define MANSECTION	3

#include "../Cakefile.defs"

#if !defined(NO_LIBTK)

/* Since we're building a library, we can't also list a regular
 * program (like tclsh) in the PRODUCTS rule, it will confuse
 * the BUILD_LIBRARY rule in Cakefile.defs.
 * Hence the EXTRA_PRODUCTS dodge
 */

#if !defined(BUILD_LIBRARY)
#define BUILD_LIBRARY	ar r
#define TK_OPTS	--srcdir=[[pwd]]/../SRCDIR/"unix" --cache-file cache.MTYPE \
			 --exec-prefix=BASEDIR --prefix=BASEDIR \
			 --x-includes=XINCDIR --x-libraries=XLIBDIR \
			 --with-tcl=[[pwd]]/LIBTCL_DIR \
			 --disable-shared --quiet
#define PRODUCTS	[[echo libtk''TCLTK_VERS.a]]
#define	AFTER_MAKE	rm -f libtk.a ; ln -s PRODUCTS libtk.a
#else
#define TK_OPTS	--srcdir=[[pwd]]/../SRCDIR/"unix" --enable-shared \
			 --cache-file cache.MTYPE --exec-prefix=BASEDIR --prefix=BASEDIR \
			 --x-includes=XINCDIR --x-libraries=XLIBDIR \
			 --with-tcl=[[pwd]]/LIBTCL_DIR \
			 --quiet
#define PRODUCTS	[[echo libtk''TCLTK_VERS.so]]
#define	SHARED_PRODUCT  libtk''TCLTK_VERS.so''[[LIBVERS]]
#define	AFTER_MAKE	mv -f libtk''TCLTK_VERS.so SHARED_PRODUCT ; sharedliblink.sh SHARED_PRODUCT ; rm -f libtk.so ; ln -s SHARED_PRODUCT libtk.so
#endif
#define EXTRA_PRODUCTS	wish

/* Rule to see if cake is running with -s flag */
#define MINUS_S	[[echo CAKEFLAGS | tr '\040' '\012' | sed -n -e /-s/p ]]

PRODUCTS: Makefile.MTYPE cache.MTYPE
	make MINUS_S -f Makefile.MTYPE CAKEFLAGS EXTRA_PRODUCTS
	AFTER_MAKE

EXTRA_PRODUCTS: Makefile.MTYPE cache.MTYPE
	make MINUS_S -f Makefile.MTYPE CAKEFLAGS EXTRA_PRODUCTS
	AFTER_MAKE

Makefile.MTYPE cache.MTYPE:		if not exist Makefile.MTYPE or not exist cache.MTYPE
	( \C\F\L\A\G\S=\"GFLAG OPTIMIZER PROFILER\"; \
		\A\F\T\E\R\_\M\A\K\E=\"AFTER_MAKE\"; \
		\R\A\N\L\I\B=\"RANLIB\";\
		\L\D\F\L\A\G\S=\"LDFLAGS\"; \
		\B\U\I\L\D\_\L\I\B\R\A\R\Y=\"BUILD_LIBRARY\"; \
		\C\C=\"CC\"; \
		 export \C\F\L\A\G\S \R\A\N\L\I\B \L\D\F\L\A\G\S \B\U\I\L\D\_\L\I\B\R\A\R\Y \C\C \A\F\T\E\R\_\M\A\K\E; \
		 ../SRCDIR/"unix"/configure TK_OPTS; \
		../SRCDIR/fix_makefile.sh > Makefile.MTYPE)

clean&:	Makefile.MTYPE
	make -f Makefile.MTYPE clean

noprod&:
	rm -rf *.so *.a wish tktest

clobber&: Makefile.MTYPE
	make -f Makefile.MTYPE clean
	rm -f *.o libtk* wish tktest *.MTYPE tkConfig.sh config.log config.status Makefile 

install&: Makefile.MTYPE
	make -f Makefile.MTYPE install
	(cd BASEDIR/lib; AFTER_MAKE)

install-nobak&: Makefile.MTYPE
	make -f Makefile.MTYPE install
	(cd BASEDIR/lib; AFTER_MAKE)

tktest: Makefile.MTYPE
	make -f Makefile.MTYPE tktest

test&: Makefile.MTYPE
	make -f Makefile.MTYPE test

/* dltest:			if not exist dltest
	cp -r ../SRCDIR/"unix"/dltest .
*/
#else
all&:
	@echo "SRCDIR:  all done"

clean&:
	@echo "" > /dev/null

noprod&:
	@echo "" > /dev/null

clobber&:
	@echo "" > /dev/null
#endif
