#
# PictureTypeB.itcl
#
# This class defines the process necessary to build a BRL-CAD
# picture of Type "F". This is a simple line drawing done using
# rtedge.
#

#
# Required packages
#
package require Itcl
package require Itk
package require Iwidgets 3.0

namespace eval RtWizard {
  
    package require PictureTypeBase 1.0
    package provide PictureTypeB 1.0

    #
    # Required packages
    #

    # None!

    #
    # The PictureTypeB class supports a line drawing.
    #
    ::itcl::class PictureTypeB {
	inherit ::RtWizard::PictureTypeBase

	public {
	    constructor {args} {
		PictureTypeBase::constructor $args
	    } {}
	
	    #
	    # Methods that impliment abstract base class methods.
	    # Unfortunately, I have to redeclare them here. 
	    #
	    method activate {}
	    method deactivate {}
	    method preview {}
	    method fullSize {}
	}

	private {
	    variable lp
	}
    }

    #--------------------#
    #   Public Methods   #
    #--------------------#    
    #
    # activate - called when this picture type is selected.
    #
    itcl::body PictureTypeB::activate { } {
	#
	# Open a new steps menu
	#
	$::wizardInstance openSteps

	#
	# Update the introductory text
	#
	$::introInstance configure -greeting "$introText"

	#
	# Activate the pages that are necessary for this picture
	# type.
	#
	set lp [$::wizardInstance enable lines]
	[$lp component useMe] select
    }

    #
    # deactivate - called when this picture type is deselected.
    #
    itcl::body PictureTypeB::deactivate { } {
	#
	# Deactivate the pages.
	#
	$::wizardInstance disable lines

	#
	# Close the current steps menu
	#
	$::wizardInstance closeSteps
    }

  #
    # preview - generates a 1/4 size preview image, limited to on-screen
    #
    itcl::body PictureTypeB::preview { } {
	#
	# get height and width
	#
	set w [$::fbp getWidth]
	set w2 [ expr $w / 2 ]

	set h [$::fbp getHeight]
	set h2 [ expr $h / 2 ]

	#	
	# get a framebuffer
	#
	set fb [$::fbp getFrameBuffer $w2 $h2 true]

	#
	# Run rt
	#
	rtedgeCmd $lp $fb $w2 $h2

    }    
    
    #
    # fullSize - generates a fullSize picture.
    #
    itcl::body PictureTypeB::fullSize { } {
	#
	# get height and width
	#
	set w [$::fbp getWidth]
	set h [$::fbp getHeight]

	#	
	# get a framebuffer
	#
	set fb [$::fbp getFrameBuffer $w $h]

	#
	# Run rt
	#
	rtedgeCmd $lp $fb $w $h
    }

}; # end namespace











