#
# PictureTypeF.itcl
#
# This class defines the process necessary to build a BRL-CAD
# picture of Type "F". This is a Ghost Image with a full-color
# insert and lines.
#
# This is the most complicated picture type.
#

#
# Required packages
#
package require Itcl
package require Itk
package require Iwidgets 3.0

namespace eval RtWizard {
  
    package require PictureTypeBase 1.0
    package provide PictureTypeF 1.0

    #
    # Required packages
    #

    # None!

    #
    # The PictureTypeF class supports makeing a Ghost view image overlayed
    # with a full-color insert and lines.
    #
    ::itcl::class PictureTypeF {
	inherit ::RtWizard::PictureTypeBase

	public {
	    constructor {args} {
		PictureTypeBase::constructor $args
	    } {}

	    #
	    # Methods that impliment abstract base class methods.
	    # Unfortunately, I have to redeclare them here. 
	    #
	    method activate {}
	    method deactivate {}
	    method preview {}
	    method fullSize {}
	}

	private {
	    method makeImage {fb w h}

	    variable gp
	    variable fp
	    variable lp
	}
    }

    #--------------------#
    #   Public Methods   #
    #--------------------#    
    #
    # activate - called when this picture type is selected.
    #
    itcl::body PictureTypeF::activate { } {
	#
	# Open a new steps menu
	#
	$::wizardInstance openSteps

	#
	# Update the introductory text
	#
	$::introInstance configure -greeting "$introText"

	#
	# Activate the pages that are necessary for this picture
	# type.
	#
	set gp [$::wizardInstance enable ghost]
	set fp [$::wizardInstance enable fullColor]
	set lp [$::wizardInstance enable lines]
	[$gp component useMe] select	
    }

    #
    # deactivate - called when this picture type is deselected.
    #
    itcl::body PictureTypeF::deactivate { } {
	#
	# Deactivate the pages.
	#
	$::wizardInstance disable ghost
	$::wizardInstance disable fullColor
	$::wizardInstance disable lines

	#
	# Close the current steps menu
	#
	$::wizardInstance closeSteps
    }

    #
    # preview - generates a 1/4 size preview image, limited to on-screen
    #
    itcl::body PictureTypeF::preview { } {
	#
	# get height and width
	#
	set w [$::fbp getWidth]
	set w2 [ expr $w / 2 ]

	set h [$::fbp getHeight]
	set h2 [ expr $h / 2 ]

	#	
	# get a framebuffer
	#
	set fb [$::fbp getFrameBuffer $w2 $h2 true]

	#
	# Run rt to generate the ghost image objects
	#
	makeImage $fb $w2 $h2
    }    
    
    #
    # fullSize - generates a fullSize picture.
    #
    itcl::body PictureTypeF::fullSize { } {
	#
	# get height and width
	#
	set w [$::fbp getWidth]
	set h [$::fbp getHeight]

	#	
	# get a framebuffer
	#
	set fb [$::fbp getFrameBuffer $w $h false]

	#
	# Run rt
	#
	makeImage $fb $w $h
    }

    #
    # makeImage
    #
    itcl::body PictureTypeF::makeImage {fb w h} {

	set tgi [file join / tmp [pid]_ghost.pix]
	set tfci [file join / tmp [pid]_fc.pix]
	set tmi [file join / tmp [pid]_merge.pix]
	set tbw [ file join / tmp [pid]_bw.bw ]
	set tmod [ file join / tmp [pid]_bwmod.bw ]
	set tbwpix [ file join / tmp [pid]_bwpix.pix]
	
	#
	# Get the background color
	#
	set bgHex [$fp getBackgroundColor]
	set bg [hexToRGB $bgHex]
	set bgl "=[lindex $bg 0]/[lindex $bg 1]/[lindex $bg 2]"

	#
	# Get the gamma
	#
	set gamma 12 ; #[$gp getIntensity]

	#
	# Run rt to generate the full-color version of the ghost image
	#
	rtCmd $gp $fb $w $h

	catch "exec fb-pix -F $fb -w $w -n $h > $tgi" junk
	
	#
	# Run rt to generate the insert
	#
	rtCmd $fp $fb $w $h
	catch "exec fb-pix -F $fb -w $w -n $h > $tfci" junk

	#
	# Convert to ghost image
	#
	catch "exec pix-bw $tgi > $tbw" junk

	catch "exec bwmod -a 4 -d259 -r$gamma -m255 < $tbw > $tmod" junk

	catch "exec bw-pix < $tmod > $tbwpix" junk

	catch "exec pixmatte -e $tfci $bgl $tbwpix $tfci > $tmi" junk

	#
	# Put the image to the framebuffer
	#
	catch "exec pix-fb -F $fb -w $w -n $h $tmi" junk

	#
	# Finally! Run rtedge
	#
	# XXX - may need to fix the logic for which objects are
	# edged
	#
	rtedgeCmd $lp $fb $w $h 1 1 $gp 
    }

}; # end namespace













