/************************************************************************
** Configuration definitions - included before all others
************************************************************************/

/*
 * This code was developed with the support of the United States Government,
 * and is not subject to copyright.
 *
 * $Log: conf.h,v $
 * Revision 1.1  1994/05/11 20:44:41  libes
 * Initial revision
 *
 */

/* Following may be necessary for some systems. */
/* For example, HPUX needs it to see getpwnam in express.c */
/* and some signals in error.c */
/*#define _INCLUDE_POSIX_SOURCE*/
