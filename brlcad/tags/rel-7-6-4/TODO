BRL-CAD to do list
==================

Included below is a list of things that are scheduled or desirable to
be done at some point.  Most recently added items are on top, older
topics are on the bottom.  This is not an official list, it's more a
scratch pad for the developers to use for tracking development ideas.


THESE SHOULD HAPPEN BEFORE THE NEXT RELEASE

* test and release


THESE ARE LOWER PRIORITY OR FOR FUTURE RELEASES

* support variable overrides for autogen.sh

* M-x indent-region scripts

* ensure successful build on mingw

* provide installation and post configuration support scripts

* make the DSP primitive use less memory when data is coming from
  a binunif. allocating 160 times binunif size is a bit extravagant
  (which comes from sizeof(struct dsp_bb)).

* write scripts that parse the sf.net tracker data and generate
  automatic reports for release.

* add dynamic geometry support. i.e. the ability to modify the parsed
  in-memory geometry tree on the fly via api calls (e.g. to add holes)

* convert the documentation to docbook and integrate their generation
  with the build system.

* investigate why g-iges followed by iges-g on a single box results in
  permuted vertex lists

* fix reference to a "Create Solid" submenu on pdf page 18 (page 6) of
  volume II

* write up the history of brlcad

* obliterate compilation warnings

* add support for subgeometry support to the geometry syndicator

* add xml, nff, bzw, pov, blend geometry import and export support

* add support for filesystem-based geometry collections

* add support for levels of detail


ODDITIES

* the scale structure in bn_cmd_noise_slice (bn_tcl.c) was never
  initialized. it looks like somebody has planned to extend the
  function but never carried out


THESE BREAK PROTOCOL OR ARE BACKWARDS-INCOMPATIBLE

* fix the database I/O writing to properly support the addition of new
  primitive types.  this includes modifying the major/minor code or
  combinations/regions and binary objects

* add database support for constraints, expressions, parametric
  values, construction history, and timestamping.


---
See the project task tracker for more to do items and future planning
efforts.  http://sf.net/pm/?group_id=105292

TODO items should be formated to column 70 (M-q in emacs), no tabs
