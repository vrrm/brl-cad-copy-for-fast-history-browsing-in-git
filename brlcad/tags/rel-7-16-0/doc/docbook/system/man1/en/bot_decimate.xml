<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2009 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='bot_decimate'>
  <refmeta>
    <refentrytitle>BOT_DECIMATE</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>bot_decimate</refname>
    <refpurpose>
      Reduces the number of triangles in the
      <emphasis>old_bot_primitive</emphasis> and saves the results to the 
      <emphasis>new_bot_primitive</emphasis>.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>bot_decimate</command>    
      <arg choice='opt'>-c <replaceable>maximum_chord_error</replaceable></arg>
      <arg choice='opt'>-n <replaceable>maximum_normal_error</replaceable></arg>
      <arg choice='opt'>-e <replaceable>minimum_edge_length</replaceable></arg>
      <arg choice='req'> <replaceable>new_bot_primitive</replaceable></arg>
      <arg choice='req'> <replaceable>old_bot_primitive</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para> Reduces the number of triangles in the <emphasis>old_bot_primitive</emphasis> 
    and saves the results to the <emphasis>new_bot_primitive</emphasis>.
    The reduction is accomplished through an edge decimation algorithm. Only changes that do not
    violate the specified constraints are performed. The <emphasis>maximum_chord_error</emphasis>
    parameter specifies the maximum distance allowed between the original surface and
    the surface of the new BOT primitive in the current editing units. The
    <emphasis>maximum_normal_error</emphasis> specifies the maximum change in surface normal (degrees)
    between the old and new surfaces. The <emphasis> minimum_edge_length</emphasis> specifies 
    the length of the longest edge that will be decimated. At least one constraint must be 
    supplied. If more than one constraint is specified, then only operations that satisfy all 
    the constraints are performed.
    </para>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      These examples demonstrate the use of the <command>bot_decimate</command> command to 
      create new primitives by reducing the number of triangles within certain constraints. 
    </para>
    <example>
      <title>Decimate Specifying Chord Error and Max Normal Error</title>
      <para>
	Creating a new BOT primitive by reducing the number of triangles in the old BOT primitive 
	and specifying the maximum chord error and maximum normal error between the old and new 
	primitives.
      </para>
      <variablelist>
	<varlistentry>
	  <term><prompt>mged></prompt> <userinput>bot_decimate -c 0.5 -n 10.0 bot.new abot</userinput></term>
	  <listitem>
	    <para>
	      Creates a new BOT primitive named <emphasis>bot.new</emphasis>by reducing the number 
	      of triangles in <emphasis>abot</emphasis> while keeping the resulting surface within 
	      0.5 units of the surface of <emphasis>abot</emphasis> and keeping the surface normals 
	      within 10 degrees.
	    </para>
	    <para>
	      Note that the constraints specified only relate the output BOT primitive to the input
	      BOT primitive for a single invocation of the command. Repeated application of this
	      command on its own BOT output will result in a final BOT primitive that has
	      unknown relationships to the original BOT primitive. For example: 
	    </para>
	  </listitem>
	</varlistentry>
	<varlistentry>
	  <term><prompt>mged></prompt> <userinput>bot_decimate -c 10.0 bot_b bot_a</userinput></term>
	</varlistentry>
	<varlistentry>
	  <term><prompt>mged></prompt> <userinput>bot_decimate -c 10.0 bot_c bot_b</userinput></term>
	  <listitem>
	  <para></para>
	    <para>
	      This sequence of commands will produce primitive <emphasis>bot_c</emphasis>with 
	      up to 20.0 units of chord error between <emphasis>bot_a</emphasis> and 
	      <emphasis>bot_c</emphasis>.
	    </para>
	  </listitem>
	</varlistentry>
	<varlistentry>
	  <term><prompt>mged></prompt> <userinput>bot_decimate -c 10.0 bot_b bot_a</userinput></term>
	</varlistentry>
	<varlistentry>
	  <term><prompt>mged></prompt> <userinput>bot_decimate -n 5.0 bot_c bot_b</userinput></term>
	  <listitem>
	    <para></para>
	    <para>This sequence of commands will produce primitive <emphasis>bot_c</emphasis> with 
	    no guaranteed relationships to <emphasis>bot_a</emphasis>. </para>
	  </listitem>
	</varlistentry>
      </variablelist>
    </example>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>

