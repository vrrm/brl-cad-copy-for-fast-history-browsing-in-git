<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='units'>

<refmeta>
  <refentrytitle>UNITS</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD MGED Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>units</refname>
  <refpurpose>With no arguments, will return the current type of units that
MGED is using.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>units</command>    
    
    <arg><replaceable>units_type</replaceable></arg>
    
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>With no arguments, will return the current type of units that
MGED is using. If a <emphasis>units_type</emphasis> is specified, MGED will switch to editing in the
indicated units. The actual database is always stored in millimeters, and the display is
adjusted to the user's choice of units. If the <emphasis>units_type</emphasis> specified on the command line
is one of the types allowed, it will be written to the database file as the preferred units
and succeeding invocations will use those units. The <emphasis>units_type</emphasis> strings that will be
remembered as the preferred editing unit are as follows:</para>
  
  <itemizedlist mark='bullet'>
<listitem>
<para>mm -- millimeters
  </para>
</listitem>

<listitem>
<para>millimeter -- millimeters
  </para>
</listitem>

<listitem>
<para>cm -- centimeters
  </para>
</listitem>

<listitem>
<para>centimeter -- centimeters
  </para>
</listitem>

<listitem>
<para>m -- meters
  </para>
</listitem>

<listitem>
<para>meter -- meters
  </para>
</listitem>

<listitem>
<para>in -- inches
  </para>
</listitem>

<listitem>
<para>inch -- inches
  </para>
</listitem>

<listitem>
<para>ft -- feet
  </para>
</listitem>

<listitem>
<para>foot -- feet
  </para>
</listitem>

<listitem>
<para>feet -- feet
  </para>
</listitem>

<listitem>
<para>um -- micrometers
  </para>
</listitem>
</itemizedlist>

<para>
<emphasis>Units_type</emphasis> strings that may be used, but will not be remembered as the preferred
editing units, are as follows:
</para>

<itemizedlist mark='bullet'>
<listitem>
<para>angstrom
</para>
</listitem>

<listitem>
<para>decinanometer
</para>
</listitem>

<listitem>
<para>nanometer
</para>
</listitem>

<listitem>
<para>nm
</para>
</listitem>

<listitem>
<para>micron
</para>
</listitem>

<listitem>
<para>micrometer
</para>
</listitem>

<listitem>
<para>km
</para>
</listitem>

<listitem>
<para>kilometer
</para>
</listitem>

<listitem>
<para>cubit
</para>
</listitem>

<listitem>
<para>yd
</para>
</listitem>

<listitem>
<para>yard
</para>
</listitem>

<listitem>
<para>rd
</para>
</listitem>

<listitem>
<para>rod
</para>
</listitem>

<listitem>
<para>mi
</para>
</listitem>

<listitem>
<para>mile
</para>
</listitem>

<listitem>
<para>nmile
</para>
</listitem>

<listitem>
<para>nautical mile
</para>
</listitem>

<listitem>
<para>au
</para>
</listitem>

<listitem>
<para>astronomical unit
</para>
</listitem>

<listitem>
<para>lightyear
</para>
</listitem>

<listitem>
<para>pc
</para>
</listitem>

<listitem>
<para>parsec
</para>
</listitem>
</itemizedlist>

</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The example shows the use of the <command>units</command> command with a <emphasis>units_type</emphasis> argument to switch 	the editing units. 
   </para>
  
      
  <example>
    <title>Switch the editing units to inches.</title>
    <para>
      <prompt>mged></prompt><userinput>units in</userinput>
    </para>
    <para>Switch to editing in inches and remember this as the preferred editing units for
	this database.       
    </para>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

