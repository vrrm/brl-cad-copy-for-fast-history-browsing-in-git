<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='rtcheck'>

<refmeta>
  <refentrytitle>RTCHECK</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD MGED Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>rtcheck</refname>
  <refpurpose>Executes the BRL-CAD <emphasis>rtcheck</emphasis> program with the default
options of "-s50  M."</refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>rtcheck</command>    
      <arg><replaceable>options</replaceable></arg>
    </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Executes the BRL-CAD <emphasis>rtcheck</emphasis> program with the default
options of "-s50  M." The -M option tells <emphasis>rtcheck</emphasis> to read the viewing parameters from standard input so that rays are only fired from the current view. The current
database name and the list of currently displayed objects are added to the end of the
<emphasis>rtcheck</emphasis> command line. The <emphasis>rtcheck</emphasis> program is written such that options may be repeated, and the last occurrence of an option will override any earlier occurrences.  This allows the user to specify other <emphasis>size (-s)</emphasis> options. The <command>rrt</command> command performs a similar function, but may be used to execute other programs as well. The <emphasis>rtcheck</emphasis> program uses raytracing to check for overlapping regions in the list of objects passed on the command line. When invoked from within MGED, any discovered overlaps along a ray are represented as yellow lines that extend only in the areas of overlap. Details and a count of overlaps are also reported. Note that overlaps of less than 0.1 mm are currently ignored by <emphasis>rtcheck</emphasis>. The default option of <emphasis>-s50</emphasis> indicates that the checking rays should be fired from a uniform square grid with 50 rays on a side. This is very coarse and may miss significant overlaps. It is recommended that the user select appropriate options for the <emphasis>rtcheck</emphasis> program and execute it for a variety viewing aspects to perform a thorough check. The granularity of the grid may be controlled with the <emphasis>-s, -w, -n, -g,</emphasis> and <emphasis>-G</emphasis> options. See the <emphasis>man</emphasis> page on <emphasis>rtcheck</emphasis> for details.
  
  </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The example shows the use of the <command>rtcheck</command> command to run the <emphasis>	rtcheck</emphasis> program with rays fired from a uniform grid with the rays spaced
	every 10 mm.    
  </para>
       
  <example>
    <title>Run the <emphasis>rtcheck</emphasis> program to fire rays from a uniform grid with the 	rays spaced a specified distance apart.</title>
    <para>
      <prompt>mged></prompt><userinput>rtcheck -g10 -G10</userinput>
    </para>
    <para>Runs the <emphasis>rtcheck</emphasis> program with rays fired from a uniform grid with 	the rays spaced every 10 mm.       
    </para>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

