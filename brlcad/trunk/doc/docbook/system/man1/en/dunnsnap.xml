<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- lifted from troff+man by doclifter -->
<refentry xmlns='http://docbook.org/ns/docbook' version='5.0' xml:lang='en' xml:id='dunnsnap1'>
<refmeta>
    <refentrytitle>DUNNSNAP
</refentrytitle>
<manvolnum>1</manvolnum>
<refmiscinfo class='source'>BRL-CAD</refmiscinfo>
<refmiscinfo class='manual'>BRL-CAD</refmiscinfo>
</refmeta>

<refnamediv>
<refname>dunnsnap</refname>
<refpurpose>expose film in a Dunn Model 631 camera</refpurpose>
</refnamediv>
<!-- body begins here -->
<refsynopsisdiv xml:id='synopsis'>
<cmdsynopsis>
  <command>dunnsnap</command>
    <arg choice='opt'>-F <replaceable>framebuffer</replaceable></arg>
    <arg choice='opt'><arg choice='plain'>- </arg><arg choice='opt'><replaceable>sS</replaceable></arg><arg choice='plain'><replaceable>squarescrsize</replaceable></arg></arg>
    <arg choice='opt'><arg choice='plain'>- </arg><arg choice='opt'><replaceable>wW</replaceable></arg><arg choice='plain'><replaceable>scr_width</replaceable></arg></arg>
    <arg choice='opt'><arg choice='plain'>- </arg><arg choice='opt'><replaceable>nN</replaceable></arg><arg choice='plain'><replaceable>scr_height</replaceable></arg></arg>
    <arg choice='opt'>-h </arg>
    <arg choice='opt'><replaceable>num_frames</replaceable></arg>
</cmdsynopsis>
</refsynopsisdiv>


<refsect1 xml:id='description'><title>DESCRIPTION</title>
<para><emphasis remap='I'>Dunnsnap</emphasis>
checks the status of the Dunn Instruments Model 631 camera and
then attempts to take
<emphasis remap='I'>num_frames</emphasis>
exposures.  If
<emphasis remap='I'>num_frames</emphasis>
is not supplied, the number of frames exposed defaults to one.
Status of the camera is checked before every exposure.
Limits on
<emphasis remap='I'>num_frames</emphasis>
include your film budget and the mean time between
failures of your computer, framebuffer and camera.</para>

<para>By default, the
<command>dunnsnap</command>
command operates independently of any framebuffer.
The
<option>-F</option>
flag causes the named framebuffer to be opened before exposure,
and closed afterwards.
This is intended for use with display systems like the SGI 4-D,
which always run a window manager.
The particular framebuffer options used could, for example,
cause the image to blanket the whole screen, or
change the video output to some other frequency.
The behavior of the framebuffer options are specific to the
type of device being used.</para>

<para>The
<option>-w </option><replaceable>scr_width</replaceable>
and
<option>-W </option><replaceable>scr_width</replaceable>
flags specifies the width of each scanline for the display device,
in pixels.</para>

<para>The
<option>-n </option><replaceable>scr_height</replaceable>
and
<option>-N </option><replaceable>scr_height</replaceable>
flags specifies the height of each scanline for the display device.</para>

<para><option>-s </option><replaceable>squarescrsize</replaceable>
and
<option>-S </option><replaceable>squarescrsize</replaceable>
set both the height and width to the size given.</para>

<para>The
<option>-h</option>
flag causes the frame buffer to be
used in high resolution mode (1024x1024).
This is important for frame buffers such as the Adage which operate
differently depending on the display size.  Without this flag
the default size for the selected device will be used (usually
the entire screen).</para>

<para>The Dunn camera has been successfully operated while attached to
an Adage (Ikonas) RDS-3000 framebuffer in both low-res (512<superscript>2</superscript> 60 Hz)
and high-res (1024<superscript>2</superscript> 30 Hz) modes.
The</para>

<para>The Dunn camera has also been successfully operated while attached to
an SGI 4-D with a 4-wire RS-343 connection when the video is operated
at 30 Hz.
The picture should be loaded into /dev/sgi5 (Full screen, Shared memory),
and then exposed with:</para>

<literallayout remap='.nf'>
dunnsnap -F /dev/sgi13 1
</literallayout> <!-- .fi -->

<para>Where /dev/sgi13 represents 30 Hz, Full Screen, and Shared memory bits.
This keeps the main SGI display watchable while the images are loaded.</para>
</refsect1>

<refsect1 xml:id='diagnostics'><title>DIAGNOSTICS</title>
<para>Diagnostics are intended to be self-explanatory.</para>
</refsect1>

<refsect1 xml:id='files'><title>FILES</title>
<variablelist remap='TP'>
  <varlistentry>
  <term><emphasis remap='I'> /dev/camera </emphasis></term>
  <listitem>
<para>camera device supported by the operating system</para>
  </listitem>
  </varlistentry>
</variablelist>
</refsect1>

<refsect1 xml:id='see_also'><title>SEE ALSO</title>
<para><citerefentry><refentrytitle>brlcad</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>dunncolor</refentrytitle><manvolnum>1</manvolnum></citerefentry></para>

</refsect1>

<refsect1 xml:id='author'><title>AUTHOR</title>
<para>BRL-CAD Team</para>

</refsect1>

<refsect1 xml:id='copyright'><title>COPYRIGHT</title>
<para>This software is Copyright (c) 1989-2013 by the United States
Government as represented by U.S. Army Research Laboratory.</para>
</refsect1>

<refsect1 xml:id='bug_reports'><title>BUG REPORTS</title>
<para>Reports of bugs or problems should be submitted via electronic
mail to &lt;devs@brlcad.org&gt;.</para>
</refsect1>
</refentry>

