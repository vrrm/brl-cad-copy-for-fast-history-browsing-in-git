///////////////////////////////////////////////////////////
//  PlaceboMsgStoppableClass.cpp
//  Implementation of the Class PlaceboMsgStoppableClass
//  Created on:      06-Aug-2008 8:00:29 AM
//  Original author: david.h.loman
///////////////////////////////////////////////////////////

#include "PlaceboMsgStoppableClass.h"


PlaceboMsgStoppableClass::PlaceboMsgStoppableClass(){

}



PlaceboMsgStoppableClass::~PlaceboMsgStoppableClass(){

}





PlaceboMsgStoppableClass::PlaceboMsgStoppableClass(String Name, int msgsToSend, long delay){

}


PlaceboMsgStoppableClass::PlaceboMsgStoppableClass(String Name, int msgsToSend, long delay, MessagingSystem ms){

}


MsgStop PlaceboMsgStoppableClass::getMsgStop(){

	return  NULL;
}


boolean PlaceboMsgStoppableClass::getRunCmd(){

	return  NULL;
}


boolean PlaceboMsgStoppableClass::getRunStatus(){

	return  NULL;
}


Thread PlaceboMsgStoppableClass::getThread(){

	return  NULL;
}


void PlaceboMsgStoppableClass::run(){

}


void PlaceboMsgStoppableClass::SendMoreMsgs(int i){

}


void PlaceboMsgStoppableClass::start(){

}


void PlaceboMsgStoppableClass::stop(){

}