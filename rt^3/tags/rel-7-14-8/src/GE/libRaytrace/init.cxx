#include <iostream>

namespace Raytrace {
  int init() {
    std::cout << "initializing libRaytrace" << std::endl;
    return 0;
  }
}

// Local Variables: ***
// mode: C++ ***
// tab-width: 8 ***
// c-basic-offset: 2 ***
// indent-tabs-mode: t ***
// End: ***
// ex: shiftwidth=2 tabstop=8
