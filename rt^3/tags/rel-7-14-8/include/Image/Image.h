#ifndef __IMAGE_H__
#define __IMAGE_H__

namespace Image {
  int init();

}
#include "Image/Pixel.h"
#include "Image/PixelImage.h"


#endif  /* __IMAGE_H__ */

// Local Variables: ***
// mode: C++ ***
// tab-width: 8 ***
// c-basic-offset: 2 ***
// indent-tabs-mode: t ***
// End: ***
// ex: shiftwidth=2 tabstop=8
