///////////////////////////////////////////////////////////
//  ConfigLoader.h
//  Implementation of the Class ConfigLoader
//  Created on:      04-Dec-2008 8:26:38 AM
//  Original author: Dave Loman
///////////////////////////////////////////////////////////

#if !defined(__CONFIGLOADER_H__)
#define __CONFIGLOADER_H__

class ConfigLoader
{

public:
	ConfigLoader();
	virtual ~ConfigLoader();

};
#endif // !defined(__CONFIGLOADER_H__)

// Local Variables: ***
// mode: C++ ***
// tab-width: 8 ***
// c-basic-offset: 2 ***
// indent-tabs-mode: t ***
// End: ***
// ex: shiftwidth=2 tabstop=8
