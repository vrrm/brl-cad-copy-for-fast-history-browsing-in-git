/*
-----------------------------------------------------------------------------
This source file is part of the Right Brain Games GUI
For the latest info, see http://www.rightbraingames.com/

Copyright (c) 2000-2007 Right Brain Games Inc.

This sample program may be used in any way you want. It is not covered by the same
license as the Right Brain Games GUI.
-----------------------------------------------------------------------------
*/

#include "Application.h"

int main( int argc, char* argv[ ] )
{
	Application app;

	//try
	//{
		app.initialize( );
		app.run( );
	//}
	//catch ( ... )
	//{
	//	return 1;
	//}

	return 0;
}
