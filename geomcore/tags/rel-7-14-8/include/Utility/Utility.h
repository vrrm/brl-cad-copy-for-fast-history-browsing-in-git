#ifndef __UTILITY_H__
#define __UTILTIY_H__

namespace Utility {
  int init();
}

#include "Utility/Application.h"
#include "Utility/Date.h"
#include "Utility/FileParser.h"
#include "Utility/Time.h"
#include "Utility/Timer.h"


#endif  /* __UTILITY_H__ */

// Local Variables: ***
// mode: C++ ***
// tab-width: 8 ***
// c-basic-offset: 2 ***
// indent-tabs-mode: t ***
// End: ***
// ex: shiftwidth=2 tabstop=8
