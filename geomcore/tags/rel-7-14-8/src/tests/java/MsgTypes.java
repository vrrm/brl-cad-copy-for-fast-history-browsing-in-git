public final class MsgTypes {
	public final static int RemHostNameSET = 0;
	public final static int RemHostNameSETFAIL = 5;
	public final static int RemHostNameSETOK = 10;
	public final static int DisconnectREQ = 15;
	public final static int NewHostOnNetINFO = 20;
	public final static int FullHostListREQ = 25;
	public final static int FullHostListREQFAIL = 30;
	public final static int FullHostListREQOK = 35;
	public final static int NewSessionREQ = 40;
	public final static int NewSessionREQFAIL = 45;
	public final static int NewSessionREQOK = 50;
	public final static int GeometryREQ = 100;
	public final static int GeometryREQFAIL = 105;
	public final static int GeometryMANIFEST = 110;
	public final static int GeometryCHUNK = 115;

}
